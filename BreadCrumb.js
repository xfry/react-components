import React from "react";
import {Link, browserHistory } from "react-router";
import ApplicationStore from '../stores/ApplicationStore';
import Utils from '../utils/AppUtils';

const breadCrumbStyle = {
  margin: '10px',
  padding: '10px'
};

class BreadCrumb extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			appName:''
		}

		this.setBreadCrumbState = this.setBreadCrumbState.bind(this);
	}

	componentWillMount() {
		ApplicationStore.on('change', this.setBreadCrumbState);
	}
	
	setBreadCrumbState() {
		this.setState({
			//appName: ApplicationStore.currentApp.name
			appName: ApplicationStore.currentApp.name
		});
	}

	componentWillUnmount() {
		ApplicationStore.removeListener('change', this.setBreadCrumbState);
	}

	render() {
		return (
			<nav aria-label="You are here:" role="navigation" style={breadCrumbStyle}>
	      <ul className="breadcrumbs">
	        <li ><Link to='/app'> Home </Link></li>
	        <li><Link to="/app">Scenarios</Link></li>
	        {/*<li className="disabled"></li>*/}
	        <li>
	          <span className="show-for-sr">Current: </span> {Utils.cutString(this.state.appName, 30, 30)}
	        </li>
	      </ul>
	    </nav>); 
	}	
}

export default BreadCrumb;