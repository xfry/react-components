import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import * as AppListActions from '../actions/AppListActions';
import dispatcher from '../dispatcher/AppDispatcher';
import ApplicationStore from '../stores/ApplicationStore';
import VmStore from '../stores/VmStore';
import UserStore from '../stores/UserStores';

class UpdateAppBtn extends React.Component {
	constructor(props) {
		super(props);
		this.currentApp = null;
		this.state = {
			disabled: true,
			isPublished: false
		};
		
		this.pendingUpdate = false;
	};

	_onTouchTap(e) {
		if(e.type==="mouseup" && this.pendingUpdate) {
			this.publishAppImplication();
		} else if(e.type==="mouseup" && this.state.isPublished === false) {
			console.log('vm store verification: ', ApplicationStore.appMessages);
			this.publishAppImplication();
		}
	}

	activateSaveLayer() {
		if(document.querySelector('.layer')) {
			document.querySelector('.layer').setAttribute('style', "display: block");
		}
	}

	hideSaveLayer() {
		if(document.querySelector('.layer')) {
			document.querySelector('.layer').setAttribute('style', "display: none");
		}
	}
	
	startUpdateApp(action) {
		//POST https://cloud.ravellosystems.com/api/v1/applications/79627482/publishUpdates?startAllDraftVms=true
		
		switch(action.type) {
			case 'SAVE_FORM_DONE':

					ApplicationStore.currentApp = action.data;
					//ApplicationStore.emit('change');
					this.postAppCost();
				break;
		}
	}
	
	postAppCost() {
		//POST https://cloud.ravellosystems.com/api/v1/applications/79627482/calcPrice;deployment
    this.activateSaveLayer();
		let auth = UserStore.userData().auth;
    const requestData = {
      'method': 'post',
      'path': `applications/${Number(this.props.currentAppId)}/calcPrice;deployment`,
      'auth': auth,
      'data': ApplicationStore.currentApp
    }
    
		//after the next line Veryfy pending update in action
		AppListActions.postAppCost(requestData);
	}

	getPendingAppUpdate(action) {
		//GET https://cloud.ravellosystems.com/api/v1/applications/79627482/pendingUpdates
    
		let auth = UserStore.userData().auth;
    let requestData = {
      'method': '',
      'path': '',
      'auth': auth
    }

		switch(action.type) {
			case "RECEIVED_APP_COST":

			    this.activateSaveLayer();
					requestData.url = 'get';
			    requestData.path = `applications/${Number(ApplicationStore.appId)}/pendingUpdates`;

			    AppListActions.getPendingAppUpdate(requestData);
				break;
			case 'RECEIVED_PENDING_UPDATES':
					this.hideSaveLayer();
					if(action.data.length) {
						this.setState({disabled: false});
						this.pendingUpdate = true;
					}

					this.isWaitingForPublish();
				break;
			case 'RECEIVED_UPDATE_APP':
					this.pendingUpdate = false;
					AppListActions.pendingUpdates = null;
					this.setState({disabled: true});
				break;
			case 'ERROR_RECEIVING_APP_STATUS_PUBLICATION':
					//remember show modal with errors
				break;
			case 'RECEIVED_APP_STATUS_PUBLICATION':
					
					this.setState({
						isPublished: action.data.value
					});

					console.log('isPublished ', this.state.isPublished +" and disabled ", this.state.disabled );
				break;
		}
	}
	
	publishAppImplication() {
		let auth = UserStore.userData().auth;
    const requestData = {
      'method': '',
      'path': '',
      'auth': auth,
    }

		if(this.pendingUpdate) {
	  	
	  	requestData.method 	= 'post';
      requestData.path 		= `applications/${Number(ApplicationStore.currentApp.id)}/publishUpdates?startAllDraftVms=true`;
      requestData.data 		= ApplicationStore.currentApp;

			AppListActions.updateApp(requestData);
		} else if(this.state.isPublished === false) {
			requestData.method 	= 'post';
      requestData.path 		= `applications/${Number(ApplicationStore.currentApp.id)}/publish`;
      requestData.data 		= ApplicationStore.currentApp;

      /*{
				id:"88609111"
				optimizationLevel:"COST_OPTIMIZED"
				startAllVms:true
			}*/

      AppListActions.publishApp(requestData);
		}
	}

	isWaitingForPublish() {
		let auth = UserStore.userData().auth;
    const requestData = {
      'method': 'get',
      'path': `applications/${Number(this.props.currentAppId)}/isPublished`,
      'auth': auth,
    }

    AppListActions.verifyAppStatus(requestData);
	}

	componentWillMount() {
		this.token1 = dispatcher.register(this.startUpdateApp.bind(this));
		this.token2 = dispatcher.register(this.getPendingAppUpdate.bind(this));
	}

	componentWillUnmount() {
		dispatcher.unregister(this.token1);
		dispatcher.unregister(this.token2);
	}

	componentDidMount() {

	    this.activateSaveLayer();
			let auth = UserStore.userData().auth;

	    const requestData = {
	      'method': 'get',
	      'path': `applications/${Number(this.props.currentAppId)}/pendingUpdates`,
	      'auth': auth
	    }

	    AppListActions.getPendingAppUpdate(requestData);
	}

	render() {
		return (
				<div className="button_container"
							style={{marginTop: '10px', textAlign: 'left'}}>
					<RaisedButton label={ 
																(this.state.isPublished === false ?  'Publish' : 'Update Scenario') || 
																(this.state.isPublished && this.state.disabled ?  'Update Scenario' : 'Update Scenario')
															}
												backgroundColor={ this.state.isPublished ?  '#84b3ff' : '#ffa500'}
												style={{color: '#f7f3f3 !important'}}
												disabled={
													(this.state.isPublished === false ? this.state.isPublished : this.state.disabled ) ||
													(this.state.isPublished && this.state.disabled === false  ? this.state.disabled : false) ||
													(this.state.isPublished && this.state.disabled ? false : false)
												}
      									onTouchTap={this._onTouchTap.bind(this)} />
				</div>
			);
	}
}

export default UpdateAppBtn;