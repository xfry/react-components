import React from 'react';
import ReactDOM from 'react-dom';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import DatePicker from 'material-ui/DatePicker';
import dispatcher from '../dispatcher/AppDispatcher';
import FontIcon from 'material-ui/FontIcon';
import {blue500, red500, greenA200} from 'material-ui/styles/colors';

/**
 * Dialogs can be nested. This example opens a Date Picker from within a Dialog.
 */
class GeneralModal extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      open: this.props.open,
      title: this.props.title,
      description: this.props.description
    };

    this.modalActions = this.modalActions.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleOpen() {
    this.setState({open: true});
  };

  modalActions(action) {
    switch(action.type) {
      
      case 'RECEIVED_PENDING_UPDATES':
          if(action.data.length) {
            //console.log('this pending description: ', action.data[0].description);
            this.setState({
              open: true,
              title: 'There Is Pending Updates',
              description: action.data[0].description
            });
          }
        break;
      
      case 'ERROR_RECEIVING_APP_STATUS_PUBLICATION':
          //console.log('Dispara Modal');
        break;

      case 'RECEIVED_APP_STATUS_PUBLICATION':
          //console.log('APP STATUS: ', action.data );
        break;
      case 'THROW_MESSAGE':
          if(action.message) {
            this.setState({
              open: true,
              title: action.message.title,
              description: action.message.description
            });
          }
        break;
    }

  }

  handleClose(e) {
    this.setState({open: false});
    
    if(this.props.modalName) {
      this.props.closeModal(this.props.modalName);
    }
  };

  componentWillMount() {
    this.token1 = dispatcher.register(this.modalActions.bind(this));
  }

  componentWillUnmount() {
    dispatcher.unregister(this.token1);
  }

  render() {
    const actions = [
      <FlatButton
        label="Close"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.handleClose}
      />,
    ];

    return (
      <div>
          <Dialog
            title={
              this.state.title

            }
            actions={actions}
            modal={false}
            open={this.state.open}
            onRequestClose={this.handleClose.bind(this)}
          >

          <div>{this.state.description}</div>

          </Dialog>
      </div>
    );
  }
}

export default GeneralModal;