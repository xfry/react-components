import React from 'react';
import ReactDOM from 'react-dom';
import dispatcher from '../dispatcher/AppDispatcher';
import * as LoginActions from '../actions/LoginActions';
import * as AppListActions from '../actions/AppListActions';
import { Link, browserHistory, hashHistory } from 'react-router';
import UserStores from '../stores/UserStores';
import loadingGif from '../assets/images/spin.gif';

import $ from 'jquery';

class LoginForm extends React.Component {
  constructor(props) {
    super(props);

    if(localStorage.getItem('logged') == 'true') {
      //browserHistory.push('app');
      //verify if current route is login and redirect to app;
      hashHistory.push('/app');
    }

    this.state = {
      loading: false
    };
    this.login = false;
    this._onSubmit    = this._onSubmit.bind(this);
    this.setLogingState = this.setLogingState.bind(this);
  }

  componentWillMount() {
    this.token = dispatcher.register(this.loginActions.bind(this));
  }

  componentDidMount() {
    UserStores.on('change', this.setLogingState);
    console.log('refs: ', ReactDOM.findDOMNode(this.refs.loadingGif) );
    document.getElementById("main").setAttribute("style", "background-color: transparent");
    if(localStorage.getItem('logged') == 'true') {
      document.getElementById('login-Form').setAttribute('style', 'display: none');
    }
  }
  
  setLogingState() {
    this.setState({
      errorForm: UserStores.error,
      user: UserStores.userData(),
      login: UserStores.logged
    });
  }

  componentWillUnmount() {
    UserStores.removeListener('change', this.setLogingState);
    dispatcher.unregister(this.token);
  }

  loginActions(action) {
    switch(action.type) {
      case "START_LOGIN":
          this.setState({
            loading: true
          });
          break;
      case "SUBMIT_FORM":
          LoginActions.submitForm(action.request);
          break;
      case "ERROR_FORM":
          this.setState({
            errorForm: action.error,
            loading: false
          });
          break;
      case "RECEIVE_USER_DATA":
          this.setState({
            login: true,
            user: action.data
          })

          localStorage.setItem('logged', true);
          localStorage.setItem('auth', btoa(JSON.stringify(action.data.auth)));
          localStorage.setItem('user', btoa(JSON.stringify(action.data)));

          break;
      case "LOGGED_IN":
          console.log('You are logged in');
          //the login loading ended
          this.setState({
            loading: false
          });

          //get all data from next view and render the view
          let auth = UserStores.userData().auth;

          const requestData = {
            'method': 'GET',
            'path': 'applications',
            'auth': auth
          }

          //Use AppListActions to get the list of app from te API
          AppListActions.getAppList(requestData);
          //browserHistory.push('app');
          hashHistory.push('/app');
          break;
    }
  }

  _onSubmit(e) {
    e.preventDefault();
    dispatcher.dispatch({type: 'START_LOGIN'});

    let userEmail = document.getElementById('user-email').value;
    let userPasswd = document.getElementById('password').value;

    /**
     * [requestData this object follows the convention set for Request.xhr]
     * @type {Object}
     */

    let requestData = {
      method: 'POST',
      path: 'login',
      auth: {
        username: userEmail,
        password: userPasswd
      }
    }

    dispatcher.dispatch({type: 'SUBMIT_FORM', request: requestData});
  }

  validateError() {
    if(this.state.errorForm) {
      const error = this.state.errorForm;
      return (<div data-alert className="form-element_container alert-box alert radius medium-6 large-12 columns">
                <label style={{'backgroundColor':'red', margin: '10px 0'}}>
                  {String(error)}
                  <a href="#" className="close">&times;</a>
                </label>          
              </div>) 
    }
  }

  render() {

    return (
      <div ref='loginForm' className="login-container" id="login-Form">
        <form action="/"
              method="POST"
              onSubmit={this._onSubmit}
              className="small-11 medium-6 large-4 login-form" 
              id="loginForm"
              ref="form" >
          
          <div className="row">
            <div className="form-element_container medium-6 large-12 columns">
              <label htmlFor="username">Email</label>
              <input id="user-email" type="email" placeholder="" required/>
            </div>
            <div className="form-element_container medium-6 large-12 columns">
              <label htmlFor="password">Password</label>
              <input id="password" type="password" placeholder="" required/>
              <p className="help-text" id="passwordHelpText">Your password must have at least 10 characters, a number, and an Emoji.</p>
            </div>
            <div ref="loadingGif" className="loading-gif" style={{width: "100%", textAlign: "center"}}>
              {
                this.state.loading ?
                <img src={`${loadingGif}`} width="70px" /> :
                ''
              }
            </div>
            {
              this.validateError()
            }
            <div className="form-btn_container medium-6 large-12 columns">
              <button className="button">Enter</button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default LoginForm;