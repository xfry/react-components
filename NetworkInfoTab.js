import React from 'react';
import { Link } from 'react-router';
import dispatcher from '../dispatcher/AppDispatcher';
import VmStore from '../stores/VmStore';

const formData = {
	network_name: null,
	network_mac: null,
	network_deviceType: null,
	network_autoIpConfig_reservedIp: null,
	network_ipconfig_dhcp: null,
	network_ipconfig_static: null,
	network_ipconfig_externalAccessState: null,
}



class NetworkInfoTab extends React.Component {
	constructor(props) {
		super(props);
	}

	_onChange(e) {
		console.log('ha cambiado ', e.nativeEvent.target);
		if(document.querySelector('.layer')) {
			document.querySelector('.layer').setAttribute('style', "display: block");
		}

		if(e.nativeEvent.target.value !== '' && e.nativeEvent.target.value !== null && typeof e.nativeEvent.target.value !== undefined) {	
			if(e.nativeEvent.target.value === 'dhcp') {
				formData[e.nativeEvent.target.getAttribute('id')] = e.nativeEvent.target.value;
				formData['network_ipconfig_static'] = '';

			} else if(e.nativeEvent.target.value === 'static') {
				formData[e.nativeEvent.target.getAttribute('id')] = e.nativeEvent.target.value;
				formData['network_ipconfig_dhcp'] = '';
			}
		}
	}

	_onFocus(e) {
		console.log('ha cambiado ', e.nativeEvent.target);
		if(document.querySelector('.layer')) {
			document.querySelector('.layer').setAttribute('style', "display: block");
		}
	}

	_onBlur(e) {
		if(e.nativeEvent.target.value !== '' && e.nativeEvent.target.value !== null && typeof e.nativeEvent.target.value !== undefined) {
			formData[e.nativeEvent.target.getAttribute('id')] = 
																													typeof e.nativeEvent.target.value !== Number ?
																													e.nativeEvent.target.value.toLocaleLowerCase()
																													: e.nativeEvent.target.value;
			console.log('updated FormData: ', formData);

			dispatcher.dispatch({type: 'UPDATE_NETWORK', networkConnections: formData});
		}
	}

	setDefaultProps() {
		formData['network_name'] 	 						= this.props.data[0].name;
		formData['network_mac'] 							= this.props.data[0].device.mac;
		formData['network_deviceType'] 	 			=	this.props.data[0].device.deviceType;
		if(this.props.data[0].ipConfig.autoIpConfig !== undefined) {
			formData['network_autoIpConfig_reservedIp'] 			= this.props.data[0].ipConfig.autoIpConfig.reservedIp;
		}
		formData['network_ipconfig_externalAccessState'] 	= this.props.data[0].ipConfig.externalAccessState;
		console.log('temporalVM ', VmStore.temporalVm.networkConnections[0]);
	}

	componentWillMount() {
			this.setDefaultProps();
			this.token = dispatcher.register(this.networkInfoActions.bind(this));
	}

	componentWillUnmount() {
		dispatcher.unregister(this.token);
	}

	networkInfoActions(action) {
		switch(action.type) {
			case 'UPDATE_NETWORK':
					VmStore.currentFormData.networkInformation = action.networkConnections;
					
					/*formData['network_autoIpConfig_reservedIp']
					formData['network_ipconfig_externalAccessState']*/
					VmStore.temporalVm.networkConnections[0]['id'] 		= action.networkConnections.id;
					VmStore.temporalVm.networkConnections[0]['name'] 	=	formData['network_name'];
					VmStore.temporalVm.networkConnections[0]['device']['mac'] 		=	formData['network_mac'];
					VmStore.temporalVm.networkConnections[0]['device'].deviceType 	=	formData['network_deviceType'];
					VmStore.temporalVm.networkConnections[0]['ipConfig'].autoIpConfig['reservedIp'] 	=	formData['network_autoIpConfig_reservedIp'];

					console.log('VmStore updated: ', VmStore.temporalVm.networkConnections);
					break;
		}
	}

	render() {
		return (<form id="network">
							<div className="networkInformation" id={this.props.data[0].id}>
								<label htmlFor="">
									name
									<input 	id="network_name"
													type="text"
													placeholder={typeof this.props.data[0].name !== undefined && this.props.data[0].name}
													onFocus={this._onFocus.bind(this)}
													onBlur={this._onBlur.bind(this)} />
								</label>
							</div>
							
							<div>
								<label htmlFor="">
									MAC
									<input 	id="network_mac"
													type="text"
													placeholder={typeof this.props.data[0].device.mac !== undefined && this.props.data[0].device.mac}
													onFocus={this._onFocus.bind(this)}
													onBlur={this._onBlur.bind(this)} />
								</label>
							</div>
							
							<div>
								<label htmlFor="">
									Device
									<select name="" id="network_deviceType"
													onFocus={this._onFocus.bind(this)}
													onChange={this._onBlur.bind(this)} >
										<option defaultValue={typeof this.props.data[0].device.deviceType !== undefined && this.props.data[0].device.deviceType}>{typeof this.props.data[0].device.deviceType !== undefined && this.props.data[0].device.deviceType}</option>
										<option value="E1000">E1000</option>
										<option value="VMXNET3">VMXNET3</option>
										<option value="VMXNET">VMXNET</option>
										<option value="VIRTIO">VIRTIO</option>
										<option value="PCNENTRTL8139">PCNENTRTL8139</option>
										<option value="NE2000">NE2000</option>
									</select>
								</label>
							</div>
							<div>
								<label htmlFor="">
									IP Configuration
								</label>
								<label htmlFor="">
									<input 	id="network_ipconfig_dhcp"	
													name="network_ipconfig"
									 				type="radio"
									 				value="dhcp"
									 				style={{margin: '0 10px'}}
													onChange={this._onChange.bind(this) }/> 
									<label htmlFor="">DHCP</label>
									<input 	id="network_ipconfig_static"	
													name="network_ipconfig"
													type="radio"
													style={{margin: '0 10px'}}
													value="static"
												 	onChange={this._onChange.bind(this) }/>
									<label htmlFor="">Static</label>
								</label>
							</div>
							<div>
								<label htmlFor="">
									Reserved IP
									<input 	id="network_autoIpConfig_reservedIp" 
													type="text"
													onFocus={this._onFocus.bind(this) }
													onBlur={this._onBlur.bind(this) }/>
								</label>
							</div>
							<hr/>
							<div>
								<label>External Access</label>
									{
										this.props.data[0].ipConfig.externalAccessState && 
										<label>(Has external Services)</label>
									}
							</div>
						</form>);
	}
}

export default NetworkInfoTab