import React from 'react';
import ReactDOM from 'react-dom';

import SectionComponent from './SectionComponent';
import FontIcon from 'material-ui/FontIcon';
import {red500, yellow500, blue500} from 'material-ui/styles/colors';

const spanStyles = {
	float: 'left',
  textAlign: 'center',
  display: 'block',
  padding: '0 10px'
}

const colors = {
	primary: '',
	secundary: ''
}

const componentStyle =  {
	background : `${colors.primary}`,  /* Old browsers */
	background : `-moz-linear-gradient(left, ${colors.primary} 0%, ${colors.secundary} 100%)`, /* FF3.6-15 */
	background : `-webkit-linear-gradient(left, ${colors.primary} 0%, ${colors.secundary} 100%)`, /* Chrome10-25,Safari5.1-6 */
	background : `linear-gradient(to right, ${colors.primary} 0%, ${colors.secundary} 100%)` /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
}

class SliderComponent extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			primaryColor: '',
			secundaryColor: '',
			closed: false,
		}

		this.setupComponent = this.setupComponent.bind(this);
		this._onClose = this._onClose.bind(this);
	}

	_onClose(e) {
		document.querySelector(".slider-component").setAttribute("style", 'display: none');
		localStorage.setItem('closedSlider', 1);
	}

	setupComponent() {
		//set colors for element
		colors.primary = this.props.primaryColor ? this.props.primaryColor: '';
		colors.secundary = this.props.secundaryColor ? this.props.secundaryColor: '';
		componentStyle['background'] = `-webkit-linear-gradient(left, ${colors.primary} 0%, ${colors.secundary} 100%)`; /* Chrome10-25,Safari5.1-6 */
		componentStyle['background'] = `linear-gradient(to right, ${colors.primary} 0%, ${colors.secundary} 100%)`; /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		componentStyle['background'] = `linear-gradient(to right, ${colors.primary} 0%, ${colors.secundary} 100%)`; //ie6
		componentStyle.height = '250px';
		componentStyle.padding = '20px';
	}

	componentWillMount() {
		this.setupComponent();
	}

	componentDidMount() {
		let sliderComponent = ReactDOM.findDOMNode(this.refs.sliderComponent);
		let closeSlider = localStorage.getItem('closedSlider');
		if(Number(closeSlider)) {
			sliderComponent.remove();
		}
	}
	
	render() {
		console.log('component style ', componentStyle);
		return (
				<div 	className='slider-component'
							ref="sliderComponent"
							style={componentStyle}>

					<i className="fi-x-circle component__close"
						 style={{ float: 'right', 
						 					color: '#84b3ff',
						 					marginTop: '-20px',
						 					cursor: 'pointer'
						 				}}
						 onClick={this._onClose}></i>

					{/*<SectionComponent title="WELCOME TO M-HACK"
														text="Now you can create scenarios, do management through the Canvas."
														titleColor="white"
														textColor="white"
														align="center" />*/}

					<SectionComponent title="HOW IT WORKS"
														text="Create a Scenario then drag and drop every module and modify them."
														titleColor="white"
														textColor="white"
														align="center"
														childs={
															<div style={{
																textAlign: 'center',
																maxWidth: '484px',
																margin: '0 auto'
															}}>
																<span style={{
																	float: 'left',
																  textAlign: 'center',
																  display: 'block',
																  padding: '0 10px'
																}}>
																	<i className="fi-layout" style={{color: 'white'}}></i>
																	<em style={{
																		display: 'block',
																		fontSize: '13px',
																		color: '#84b3ff'
																	}}>Create your Scenario</em>
																</span>
																<span style={{
																	float: 'left',
																  textAlign: 'center',
																  display: 'block',
																  padding: '0 10px'
																}}>
																	<i className="fi-results" style={{color: 'white'}}></i>
																	<em style={{
																		display: 'block',
																		fontSize: '13px',
																		color: '#84b3ff'
																	}}>Add your module vms</em>
																</span>
																<span style={{
																	float: 'left',
																  textAlign: 'center',
																  display: 'block',
																  padding: '0 10px'
																}}>
																	<i className="fi-page-multiple" style={{color: 'white'}}></i>
																	<em style={{
																		display: 'block',
																		fontSize: '13px',
																		color: '#84b3ff'
																	}}>Drag & Drop to organize</em>
																</span>
															</div>
														} />
				</div>
			)
	}
}

export default SliderComponent;