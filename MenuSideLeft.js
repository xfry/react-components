import React from "react";
import ReactDOM from "react-dom";
import VmStore from "../stores/VmStore";
import dispatcher from '../dispatcher/AppDispatcher';
import Utils from '../utils/AppUtils';
import PropTypes from 'prop-types';

class MenuSideLeft extends React.Component{
	constructor(props) {
		super(props);
    this.state = {
      totalVmList: null,
      elOnDrag: {
        id: null,
        name: null,
        canvasX: null,
        canvasY: null,
      }
    }

    this._startDrag = this._startDrag.bind(this);
    this._endDrag   = this._endDrag.bind(this);
    this.setMenuSideState = this.setMenuSideState.bind(this);
	}

  _onChange(e) {
    let newVmsList = VmStore.searchVmByName(this.state.totalVmList, e.nativeEvent.target.value);
    this.setState({
      totalVmList: newVmsList
    });
  }

  componentWillMount() {
    //remember get totalVmList
    VmStore.on('change', this.setMenuSideState);
  }

  setMenuSideState() {
    this.setState({
      totalVmList: VmStore.getTotalVmList()
    });
  }

  _startDrag(e) {
    //
  }

  _endDrag(e) {
    const el = e.nativeEvent.target;

    VmStore.vmToDraw = {
      id: el.id,
      name: el.innerText,
      canvasX: null,
      canvasY: null
    }

    dispatcher.dispatch({type: 'DRAW_VM_ON_CANVAS', data: VmStore.vmToDraw});
  }

  componentDidMount() {
    this.menuSideLeft = ReactDOM.findDOMNode(this.refs.menuSideLeft);
    this.menuSideLeft.style = `height: ${Math.max(document.documentElement.clientHeight, window.innerHeight || 0)-200}px`
  }

  componentDidUpdate(prevProps, prevState) {
  }

  componentWillUnmount() {
    VmStore.removeListener('change', this.setMenuSideState);
  }

  formatVmList(vms) {
      let vmList = [];
      vms.sort(function(a, b){
        if(a.name > b.name) return 1;
        if(a.name < b.name) return -1;
        return 0;
      });
      
      if(vms) {
          vmList = vms.map((item, index) => {
            return (<div key={index} id={item.baseVmId} className="item-vm" draggable={true} onDragStart={this._startDrag} onDragEnd={this._endDrag} title={item.name}>
                      <i className="fi-monitor item-vm_icon" /> {Utils.cutString(item.name, 15, 15)} 
                    </div>)
          });

          return vmList;
      }
  }

  render() {
    return (
      <div className="menuSideLeft" ref="menuSideLeft">
          <label className="menuSideLeft_search">
            <header className="menuSideLeft_title">
              <h4 style={{textAlign: 'center', background: 'white', color: '#047bc0', margin: '0'}}>ARSENAL</h4>
            </header>
            <i className="fi-magnifying-glass icon-search"></i>
            <input  type="text"
                    id="search"
                    className="search"
                    placeholder="Search Vm"
                    onChange={this._onChange.bind(this)} />
          </label>
          <div  className="menuSideLeft_vms" 
                style={{
                  height: `${this.props.viewporOptions.height - 300 }px`,
                  width: `100%`,
                }}>
            {
              this.state.totalVmList ?
              this.formatVmList(this.state.totalVmList) :
              ""
            }
          </div>
      </div>
    );
  }
};

MenuSideLeft.defaultProps = {
  elOnDrag: {}
}

MenuSideLeft.propTypes = {
  elOnDrag: PropTypes.object
}

export default MenuSideLeft;