import React from "react";
import ReactDOM from "react-dom";
import { fabric }		from 'fabric-webpack';
import VmStore from '../stores/VmStore';
import dispatcher from '../dispatcher/AppDispatcher';
import UserStore from "../stores/UserStores";
import ApplicationStore from '../stores/ApplicationStore';
import * as VmActions from	'../actions/VmActions';
import * as AppListActions from	'../actions/AppListActions';
import Utils from '../utils/AppUtils';
import Request from '../utils/Request';
import GeneralModal from './GeneralModal';

let newCanvas = null;
let elPool = [];

class Canvas extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
			canvas: null,
			ctx: null,
			vmList: this.props.vmList,
			drawCanvas: false,
			isDuplicated: false,
			hasMessages: false,
			messageTitle: '',
			messageDescription: ''
		}

		this.strarDrag 			= this.strarDrag.bind(this);
		this.endDrag 				= this.endDrag.bind(this);
		this.drawElement 		= this.drawElement.bind(this);
		this.listenMousePosition = this.listenMousePosition.bind(this);
		this.hideSaveLayer = this.hideSaveLayer.bind(this);
		this.showLoadingLayer = this.showLoadingLayer.bind(this);
		this.saveVmPosition = this.saveVmPosition.bind(this);
		this.updateApplication = this.updateApplication.bind(this);
		this.closeMenuSideTabs = this.closeMenuSideTabs.bind(this);
		this.updateVmPosition = this.updateVmPosition.bind(this);
	}

	hideSaveLayer() {
		if(document.querySelector('.container_layer_loader')) {
			document.querySelector('.container_layer_loader').setAttribute('style', "display: none");
		}
	}

	showLoadingLayer() {
		if(document.querySelector('.container_layer_loader')) {
			document.querySelector('.container_layer_loader').setAttribute('style', "display: block");
		}
	}

	drawElement(vmToDraw) {
		const el = vmToDraw;
		el.left = this.props.mousePos.x;
		el.top = this.props.mousePos.y;
		
		el.item(2).set({
			fill: 'red'
		});

		let auth = UserStore.userData().auth;

    const requestData = {
      'method': 'post',
      'path': `applications/${this.props.currentAppId}/vms`,
      'auth': auth,
      'data': {
      	'baseVmId': Number(el.id)
      }
    }
		
		this.showLoadingLayer();

		const position = {
			x: el.left,
			y: el.top
		}
		
		Request.xhr(requestData, res => {
			
			let newVm = null;
			if(!res.hasOwnProperty('design')) {
				return;
			} else {
				if(res.design.hasOwnProperty('vms')) {
					newVm = res.design.vms.filter((item) => {
										if(Number(item.baseVmId) === Number(el.id)) {
											return item;
										}
									});
				}
			}

			this.setState({
				vmPosition: position
			});

			ApplicationStore.currentApp = res;
			VmStore.currentApp = res;
			VmStore.currentVm = newVm[0];
			VmStore.design = res.design;
			VmStore.vmList = res.design.vms;
			VmStore.layout = res.design.layout;

			this.props.vmList.push(newVm[0]);

			if(this.draw) {
				
				el.id = newVm[0].id;
				//console.log('Group El To Draw: ', el);
				this.state.ctx.fillStyle = "transparent";
				this.state.ctx.fillRect(el.left, el.top, el.width, el.height);
				newCanvas.add(el);
				newCanvas.renderAll();
				this.draw = false;
				this.hideSaveLayer();
				
			}

			dispatcher.dispatch({type: "CREATE_VMS_LAYOUT_AND_DRAW", data: {
				el: el,
				app: res,
				vm: newVm[0],
				position: position,
				layout: VmStore.layout
			}});
		});
	}

	updateLayoutPosition() {

	}
	
	saveVmPosition(vm, layout, position) {
		if(!ApplicationStore.currentApp.design.hasOwnProperty('layout')) {
			ApplicationStore.currentApp.design['layout'] = {};
			ApplicationStore.currentApp.design.layout['vmLayoutItem'] = [{
										    "location": {
										      "x": position.x,
										      "y": position.y
										    },
										    "vmId": vm.id
										  }];
		} else {
			ApplicationStore.currentApp.design.layout['vmLayoutItem'].push({
										    "location": {
										      "x": position.x,
										      "y": position.y
										    },
										    "vmId": vm.id
										  });
		}

		this.updateApplication(ApplicationStore.currentApp);
	}

	updateVmPosition(vm, currentLayout) {
		ApplicationStore.currentApp.design.layout['vmLayoutItem'].forEach((item, index) => {
			console.log("layout to update: ", item.vmId === vm.id ? item : '');
			if(item.vmId=== vm.id) {
				//update current vm position
				VmStore.currentVm[0].layoutLocation = {
					x: vm.left,
					y: vm.top
				}

				//update application memory
				item.location = {
					x: vm.left,
					y: vm.top
				};
			}
		});

		this.updateApplication(ApplicationStore.currentApp);
	}

	updateApplication(currentApp) {
		this.showLoadingLayer();
		let auth = UserStore.userData().auth;
    const requestData = {
      'method': 'put',
      'path': `applications/${currentApp.id}`,
      'auth': auth,
      'data': currentApp
    }
		
		//after the next line Veryfy pending update in action
		AppListActions.updateApp(requestData);
	}

	listenMousePosition(canvas){
		//this.state.canvas.getBoundingClientRect
		let rect = canvas.getBoundingClientRect();

		canvas.addEventListener('mouseover', (e) => {
			this.props.mousePos.x = e.clientX;
			this.props.mousePos.y = e.movementY;

			dispatcher.dispatch({type: "GET_MOUSE_POSITION"});
		});
	}

	canvasActions(action) {
		switch(action.type) {
			case 'CREATE_VMS_LAYOUT_AND_DRAW':
					this.saveVmPosition(action.data.vm, action.data.layout, action.data.position);
					let currentVmLayout =	VmStore.getVmLocation(VmStore.vmList, action.data.layout);
					break;
			case 'DRAW_VM_ON_CANVAS':
						//verify if vm is ducplicated then abort operation.
						let el = new fabric.Rect({
							fill: 'green',
							originX: 'center',
						  originY: 'center',
							width: 90,
						  height: 110,
						});

						let text = new fabric.Text('hello world', {
						  fontSize: 10,
							fontWeight: 'normal',
						  originX: 'center',
						  originY: 'center'
						});

						let elRect = new fabric.Rect({
							fill: 'rgb(175, 215, 122)',
							originX: 'center',
						  originY: 'center',
							width: 30,
						  height: 30,
						});

						let elRect2 = new fabric.Rect({
							fill: '#a4cbdf',
							originX: 'center',
						  originY: 'center',
						  top: 50,
							width: 90,
						  height: 10,
						});

						let group = new fabric.Group([ el, text, elRect, elRect2], {
							id: action.data.id,
							name: action.data.name,
							left: action.data.canvasX,
						  top: action.data.canvasY,
						  appId: ApplicationStore.currentApp.id,
						  hasBorders: false,
							hasControls: false,
							hasRotatingPoint: false,
						});
						
						group.item(0).set({
							fill: '#f9f9f9',
							stroke: 'rgba(23, 121, 186, 0.7)',
							strokeWidth: 2,
							strokeLineJoin: 'round'				
						});

						let a = action.data.name;
						let b = " \n";
						let position = 13;
						let output = `${[a.slice(0, position), b, a.slice(position)].join('')}`;

						group.item(1).set({
							fontFamily: 'Open Sans',
							fill: '#0f0f0f',
							text: Utils.cutString(output, 28, 28),
							top: -25,
							shadow: 'rgba(0,0,0,0.6) 1px 1px 3px',
							strokeLineJoin: 'round'
						});
						
						el.set('selectable', true);
						//console.log("GROUP OBJECT ", group);
						this.objectToDraw = group;
						this.draw = true;
					break;
			case 'GET_MOUSE_POSITION':

						if(this.draw) {

							let isDuplicated = VmStore.verifyDuplicity(this.objectToDraw.name, VmStore.vmList);
							
							this.setState({
									isDuplicated: isDuplicated
							});

							if(isDuplicated) {
								
								this.setState({
									isDuplicated: isDuplicated,
									hasMessages: isDuplicated,
									messageTitle: 'Warning',
									messageDescription: "The vm you have dropped already exist, please try with other."
								});

								this.draw = false;
								this.objectToDraw = null;
								return;
							}

							this.drawElement(this.objectToDraw);
						}

					break;
			case 'ERROR_UPDATING_APP':
					//console.log('ERROR_UPDATING_APP');
					//console.log('Error updating app:  ', action.error);
					break;
			case 'RECEIVED_UPDATE_APP':
					this.hideSaveLayer();
					//remember emmit the change
					break;
			case 'DELETE_VM_DONE':
					let canvasObjects = newCanvas.getObjects();
					let elementToDelete = canvasObjects.filter((item, index) => {
																	if(item.id === action.data[0]) {
																		return item;
																	}
																});
					newCanvas.remove(elementToDelete[0]);
					this.closeMenuSideTabs();
					this.hideSaveLayer();
					break;
		}
	}

	closeMenuSideTabs() {
		document.querySelector('.menuSideRight').setAttribute('style', 'display: none');
		if(document.querySelector('.layer')) {
			document.querySelector('.layer').setAttribute('style', "display: none");
		}
	}
 
	componentWillMount() {
		//register draw method on dispatcher
		this.setState({
			drawCanvas: this.drawCanvas
		})
		this.token = dispatcher.register(this.canvasActions.bind(this));
		//Load all elements previously saved in the canvas
	}

	componentDidMount() {
		this.canvas = ReactDOM.findDOMNode(this.refs.vmsCanvas);
		newCanvas = new fabric.Canvas(`${this.canvas.id}`);
		newCanvas.selection = true;
		newCanvas.clear();
		this.setCanvasEvents();
		newCanvas.setBackgroundColor({source: 'http://netmaniamediagroup.com/grid.jpg', repeat: 'repeat'}, newCanvas.renderAll.bind(newCanvas));

		//document.querySelector('.upper-canvas').remove();

		this.listenMousePosition(document.querySelector('.upper-canvas'));

		this.setState({
			canvas: document.querySelector('.upper-canvas'),
			ctx: this.canvas.getContext('2d'),
			vmList: this.props.vmList,
		});
	}

	autoMaticDraw(canvas) {
		console.log('AutoMaticDraw');
		if(!this.props.vmList.length) {
			/*console.log('no hay maquinas para dibujar en Canvas ');
			newCanvas.clear();*/
			return;
		}

		const ctx = canvas.getContext('2d');
		const rect = canvas.getBoundingClientRect();
		newCanvas.clear();
		this.props.vmList.forEach((item, index) => {
			if(item.layoutLocation === undefined) {
				console.log('no tiene layout location: ', item);
			}

			console.log('item layoutLocation: ', item.layoutLocation);
			//set new position for item if it's dimention is grather than canvas
			if(item.layoutLocation.x >= canvas.width || 
				 	item.layoutLocation.x <= 0) {
				item.layoutLocation.x = canvas.width * 0.5;
			}

			if(item.layoutLocation.y >= canvas.height || 
				 	item.layoutLocation.y <= 0) {
				item.layoutLocation.y = canvas.height * 0.5;
			}

			let el = new fabric.Rect({
				fill: 'green',
				originX: 'center',
			  originY: 'center',
				width: 90,
			  height: 110,
			});

			let text = new fabric.Text('hello world', {
			  fontSize: 10,
				fontWeight: 'normal',
			  originX: 'center',
			  originY: 'center'
			});
			
			let elRect = new fabric.Rect({
				fill: 'rgb(175, 215, 122)',
				originX: 'center',
			  originY: 'center',
				width: 30,
			  height: 30,
			});

			let elRect2 = new fabric.Rect({
				fill: '#a4cbdf',
				originX: 'center',
			  originY: 'center',
			  top: 50,
				width: 90,
			  height: 10,
			});

			let group = new fabric.Group([ el, text, elRect, elRect2], {
				id: item.id,
				name: item.name,
			  left: item.layoutLocation.x,
			  top: item.layoutLocation.y,
			  appId: ApplicationStore.currentApp.id,
			  hasBorders: false,
				hasControls: false,
				hasRotatingPoint: false,
			});

			group.item(0).set({
				fill: '#f9f9f9',
				stroke: 'rgba(23, 121, 186, 0.7)',
				strokeWidth: 2,
				strokeLineJoin: 'round'				
			});

			let a = item.name;
			let b = " \n";
			let position = 13; // despues de 13 caracteres haga salto de linea
			let output = `${[a.slice(0, position), b, a.slice(position)].join('')}`;

			group.item(1).set({
				fontFamily: 'Open Sans',
				fill: '#0f0f0f',
				text: Utils.cutString(output, 28, 28),
				top: -25,
				shadow: 'rgba(0,0,0,0.6) 1px 1px 3px',
				strokeLineJoin: 'round'
			});

			if(!VmStore.isVmStarted(item)) {
				group.item(2).set({
					fill: 'red'
				});
			}

			//console.log("el to Draw: ", group.left, " Y: ", group.top);
			newCanvas.setBackgroundColor({source: 'http://netmaniamediagroup.com/grid.jpg', repeat: 'repeat'}, newCanvas.renderAll.bind(newCanvas));
			newCanvas.add(group);
			if(elPool.length < this.state.vmList.length) {
				elPool.push(group);
			}
			
			window.newCanvas = newCanvas; //remember delete this line
		});

		this.setElPoolOptions(elPool);
		this.draw = false;
	}

	componentDidUpdate(prevProps, prevState) {
		if(prevProps.vmList !== null) {
			this.canvas = ReactDOM.findDOMNode(this.refs.vmsCanvas);
			newCanvas.renderAll();
			this.autoMaticDraw(this.canvas);
		}
	}

	changeScale(options) {
		//
	}

	saveCanvas() {
		//
	}

	strarDrag() {
		//
	}

	endDrag() {
		//update Vmposition
		//save new data in vms
	}

	setCanvasEvents() {
		newCanvas.on('mouse:down', (options) => {
			if(options.target === undefined) {
				return;
			}

			if(options.target.id) {
				let selectedVm = VmStore.setCurrentVm(Number(options.target.id));
				dispatcher.dispatch({type: "CURRENT_VM_SELECTED", data: selectedVm })
				document.querySelector('.menuSideRight').setAttribute('style', 'display: block');
			}
		});

		newCanvas.on('object:modified', (options) => {
			if(options.target === undefined) {
				return;
			}

			let currentLayout = VmStore.getVmLayoutInfo(VmStore.currentVm, VmStore.layout);
			//this.saveVmPosition(VmStore.currentVm[0], , position);
			this.updateVmPosition(options.target, currentLayout);

		});
	}
	
	setElPoolOptions(elpool) {
		elpool.forEach((item) => {
			item.set('selectable', true) //make selectable elements
		});
	}

	removeCanvas() {
		let canvasContainer = ReactDOM.findDOMNode(this.refs.vmsCanvas);
		ReactDOM.unmountComponentAtNode(canvasContainer);
		canvasContainer.remove();
		let upperCanvas = document.querySelector('.upper-canvas');
		upperCanvas.remove();
	}

	componentWillUnmount() {
		//console.log('original canvas? ', document.getElementById('c'));

		newCanvas.clear();
		newCanvas = null;
		this.setState({
			canvas: null,
			ctx: null,
			vmList: null,
			drawCanvas: false,
			isDuplicated: false,
			hasMessages: false,
			messageTitle: '',
			messageDescription: ''
		},() => {
			this.removeCanvas();
		});

		VmStore.resetStore();
		dispatcher.unregister(this.token);
	}

  render() {
    return (
      <div className="elements_canvas-container large-7 medium-7">
      	{
      		this.state.hasMessages &&
	      	<GeneralModal open={this.state.hasMessages} 
	                      title={this.state.messageTitle }
	                      description={this.state.messageDescription} />
      	}
      	<canvas ref="vmsCanvas" id="c" data-draw={this.draw} className="canvas-container_object" width={this.props.width} height={(this.props.height-110)} />
      </div>
    );
  }
};

Canvas.defaultProps = {
	mousePos: {
		x: 0,
		y: 0
	},
	objectToDraw: null,
	draw: false,
};

Canvas.propTypes = {
	mousePos: React.PropTypes.object,
	objectToDraw: React.PropTypes.object,
	draw: React.PropTypes.bool
};

export default Canvas;