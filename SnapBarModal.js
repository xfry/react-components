import React from 'react';
import Snackbar from 'material-ui/Snackbar';

class SnackbarModal extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: this.props.open,
    };
  }

  render() {
    return (
      <div>
        <Snackbar
          open={this.props.open}
          message={this.props.message}
          autoHideDuration={3000}
        />
      </div>
    );
  }
}

export default SnackbarModal;