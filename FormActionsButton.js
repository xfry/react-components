import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import dispatcher from '../dispatcher/AppDispatcher';
import * as TabActions from '../actions/TabActions';
import VmStore from '../stores/VmStore';
import UserStore from '../stores/UserStores';
import ApplicationStore from '../stores/ApplicationStore';

const actionStyle = {
	textAlign: 'center',
	padding: '10px',
}

const buttonStyle = {
	backgroundColor: '#1779ba'
}

class FormActionsButton extends React.Component {
	constructor(props) {
		super(props);
		this.saveAction = this.saveAction.bind(this);
	}

	_onClick(e) {
		if(e.target.innerText.toLowerCase() === "save") {
			this.saveForm();
		} else {
			this.hideSaveLayer();
		}
	}
	
	hideSaveLayer() {
		if(document.querySelector('.container_layer_loader')) {
			document.querySelector('.container_layer_loader').setAttribute('style', "display: none");
		}
	}

	showLoadingLayer() {
		if(document.querySelector('.container_layer_loader')) {
			document.querySelector('.container_layer_loader').setAttribute('style', "display: block");
		}
	}

	saveForm(){
		console.log('start to save');
		let auth = UserStore.userData().auth;
    const requestData = {
      'method': 'put',
      'path': `applications/${Number(ApplicationStore.appId)}/vms/${VmStore.temporalVm.id}`,
      'auth': auth,
      'data': VmStore.temporalVm
    }

    console.log('request data: ', requestData);
    this.showLoadingLayer();
		TabActions.saveForm(requestData);
	}

	saveAction(action) {
		//POST https://cloud.ravellosystems.com/api/v1/applications/79627482/publishUpdates?startAllDraftVms=true
		
		switch(action.type) {
			case 'SAVE_FORM_DONE':
					this.hideSaveLayer();
					console.log('App actualizada: ', action.data);
					ApplicationStore.currentApp = action.data;
					//ApplicationStore.emit('change');
					//this.postAppCost();
				break;
		}
	}

	componentWillMount() {
		this.token = dispatcher.register(this.saveAction);
	}

	componentDidMount() {

	}

	componentWillUnmount() {
		dispatcher.unregister(this.token);
	}

	render() {
		return (
				<div id="form_actions" style={actionStyle}>
					<label>
						<RaisedButton className="form-action_btn" 
													label="Save"
													id="save_btn"
													onTouchTap={this._onClick.bind(this)} />
	        	<RaisedButton className="form-btn_btn"
	        								label="Cancel"
	        								id="canvel"
	        								onTouchTap={this._onClick.bind(this)} />
					</label>
				</div>
			);
	}
}

export default FormActionsButton;