import React from 'react';

class SectionComponent extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			visible: true,
			title: '',
			text: '',
			childs: '',
			'titleColor': '',
			'textColor': '',
		}

		this.configComponent = this.configComponent.bind(this);
	}
	
	configComponent() {
		this.setState({
			title: this.props.title,
			text: this.props.text,
			childs: this.props.childs ? this.props.childs : '',
			titleColor: this.props.titleColor,
			textColor: this.props.textColor,
			align: this.props.align
		});
	}

	componentWillMount() {
		this.configComponent();
	}

	render() {
		return (
				<div className="section_component" id="section_component">
					{/*<i className="fi-x-circle component__close"></i>*/}
					<header className="component__header">
						<h1 className="component__header_title" 
								style={{
									color: this.state.titleColor,
									textAlign: this.state.align
								}}>

							{this.state.title ? this.state.title : ''}
						</h1>
					</header>
					<p className="component__text"
							style={{
								color: this.state.textColor,
								textAlign: this.state.align
							}}>
						{this.state.text ? this.state.text : ''}
					</p>
					{
						this.props.childs ?
						this.state.childs : ''
					}
				</div>
			);
	}
}

export default SectionComponent;