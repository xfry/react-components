import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import Dialog from 'material-ui/Dialog';
import * as BluePrintActions from '../actions/BluePrintActions';
import dispatcher from '../dispatcher/AppDispatcher';
import ApplicationStore from '../stores/ApplicationStore';
import VmStore from '../stores/VmStore';
import UserStore from '../stores/UserStores';
import SnapBarModal from './SnapBarModal';

class SaveBluePrintBtn extends React.Component {
	constructor(props) {
		super(props);
		this.currentApp = null;

		this.state = {
			open: false,
			disabled: false,
			name: '',
			openSnapBar: false,
			snapBarMessage: '',
		};

		this.pendingUpdate = false;
		this._openleModal = this._openleModal.bind(this);
		this._closeModal = this._closeModal.bind(this);
    this._handleTextChange = this._handleTextChange.bind(this);
    this.createBlueprintAction = this.createBlueprintAction.bind(this);
    this.createBluePrint = this.createBluePrint.bind(this);
    this._onTouchTap = this._onTouchTap.bind(this);
	};

	_onTouchTap(e) {
		this.createBluePrint();
	}

	hideSaveLayer() {
    if(document.querySelector('.container_layer_loader')) {
      document.querySelector('.container_layer_loader').setAttribute('style', "display: none");
    }
  }

  showLoadingLayer() {
    if(document.querySelector('.container_layer_loader')) {
      document.querySelector('.container_layer_loader').setAttribute('style', "display: block");
    }
  }
	
	createBlueprintAction(action) {
		//POST https://cloud.ravellosystems.com/api/v1/applications/79627482/publishUpdates?startAllDraftVms=true
		switch(action.type) {
			case 'ERROR_SAVING_BLUEPRINT':
					this.hideSaveLayer();
					this.setState({
						openSnapBar: true,
						snapBarMessage: action.error
					});
					console.log('Error Blueprint: ', action.error);
				break;
			case 'SAVED_BLUEPRINT':
					this.hideSaveLayer();
					this.setState({
						openSnapBar: true,
						snapBarMessage: "Blueprint Saved successfull"
					});
					//console.log('Blueprint Creado: ', action.data);
				break;
		}
	}
	
	createBluePrint() {
		this.showLoadingLayer();
		let auth = UserStore.userData().auth;
		
		let blueprint = {
			applicationId: ApplicationStore.currentApp.id,
			blueprintName: this.state.name,
			offline: true,
		}

    const requestData = {
      'method': 'post',
      'path': `blueprints`,
      'auth': auth,
      'data': blueprint
    }
		
		console.log('petition blueprint: ', blueprint);
		BluePrintActions.saveBluePrint(requestData);
		this.setState({
			open: false
		});

	}

	componentWillMount() {
		this.token1 = dispatcher.register(this.createBlueprintAction.bind(this));
	}

	componentDidMount() {
	}

	componentWillUnmount() {
		dispatcher.unregister(this.token1);
	}

	
	_openleModal(e){
    this.setState({open: true});
  };

  _closeModal(e){
    this.setState({open: false});
  };

  _handleTextChange(e) {
		this.setState({
			name: e.nativeEvent.target.value
		});
  }

	render() {
		
		const actions = [
      <FlatButton
        label="Ok"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this._onTouchTap}
      />,

      <FlatButton
        label="Cancel"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this._closeModal}
      />
    ];

		return (
				<div className="button_container"
							style={{	marginTop: '10px', 
												marginLeft: '10px',
												marginRight: '10px',
												textAlign: 'left' }}>
					<RaisedButton label='Save Blueprint'
												backgroundColor='#84b3ff'
												style={{color: '#f7f3f3 !important'}}
												disabled={this.state.disabled}
      									onTouchTap={this._openleModal.bind(this)} />
					<Dialog
	          title="Save As Blueprint"
	          actions={actions}
	          modal={false}
	          open={this.state.open}
	          //onRequestClose={this._handleModal} 
	          >
	          <div>
	            name: 
	            <TextField
	              id="text-field-controlled"
	              value={this.state.value}
	              onBlur={this._handleTextChange}
	            />
	          </div>
	        </Dialog>
	        <SnapBarModal open={this.state.openSnapBar} message={this.state.snapBarMessage}/>
				</div>
			);
	}
}

export default SaveBluePrintBtn;