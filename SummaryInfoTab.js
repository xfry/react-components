import React from 'react';
import { Link } from 'react-router';
import VmStore from '../stores/VmStore';
import dispatcher from '../dispatcher/AppDispatcher';

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
};

class SummaryInfoTab extends React.Component {

	constructor(props) {
		super(props);
		this.goToGeneral = this.goToGeneral.bind(this);
		this.goToSystem = this.goToSystem.bind(this);
		this.goToNetwork = this.goToNetwork.bind(this);
	}

	goToGeneral(e) {
		dispatcher.dispatch({
			type: 'GO_TO_FORM_TAB',
			tab: 2
		});
	}

	goToNetwork(e) {
		dispatcher.dispatch({
			type: 'GO_TO_FORM_TAB',
			tab: 5
		});
	}

	goToSystem(e) {
		dispatcher.dispatch({
			type: 'GO_TO_FORM_TAB',
			tab: 3
		});
	}

	render() {
		return (
				<div>
		      <h2 style={styles}>{this.props.data ? this.props.data.name : ''}</h2>
		      <span className="machine-nane">{this.props.data ? this.props.data.name: ''}</span>
		      <hr />
		      <div>
		      	<Link onClick={this.goToSystem} > 
		      			<span className="machine-cpus">{this.props.data ? `${this.props.data.numCpus} CPUs` : ''}</span>
		      	</Link> |
		      	<Link onClick={this.goToSystem} >
		      			<span className="machine-memory">{this.props.data ? `${this.props.data.memorySize.value} ${this.props.data.memorySize.unit} Memory` : ''}</span>
		      	</Link> |
		        <Link onClick={this.goToSystem}>
		        		<span className="machine-hardDisk">{this.props.data ? `${this.props.data.size.value} ${this.props.data.size.unit} Total Storage` : ''}</span>
		        </Link> |
		      </div>

		      <div>
		      	{
		      		this.props.data.hostnames ?
		      		<div>HostName: <Link onClick={this.goToGeneral}>
		      													{this.props.data.hostnames}
		      									 </Link>
		      		</div> :
		      		""
		      	}
		      	{
		      		this.props.data.keypairName !== undefined ?
		      		<div>KeyPair: <Link onClick={this.goToGeneral}>
		      												{this.props.data.keypairName}
		      									</Link>
		      		</div> :
		      		''
		      	}
		      </div>
		      <hr />
		      <Link onClick={this.goToNetwork}> <span className="machine-network">{this.props.data ? `${this.props.data.netWorkLength} Network Connection` : ''}</span></Link>
		      <div className="machine-network_information" id="machine-network_information">
		      	<div className="network_information_vm" 
		      				style={{
		      					border: '1px solid rgba(80,80,80,0.2)',
    								margin: '10px 0',
    								borderRadius: '2px'
		      				}}>
		      	<label className="network_information__vm-state" >
		      		<span>
		      			<i 	className={VmStore.isVmStarted(this.props.data.vm) ? "fi-play": "fi-stop"}
		      					style={VmStore.isVmStarted(this.props.data.vm) ? 
		      									{color: 'green', margin:'0 10px', fontSize: '25px'} : 
		      									{color: 'red', margin:'0 10px', fontSize: '25px'}
		      								}></i>
		      		</span>
		      		{
		      			VmStore.isVmStarted(this.props.data.vm) ? 
		      			<spa><label>Vm Started</label></spa> : 
		      			<spa><label>Vm Stopped</label></spa>
		      		}
		      		<div className="public_ip">
			      		{
			      				VmStore.getPublicId(this.props.data.vm) ?
				      			<label className="public_ip_text"
				      							style={{margin: '5px'}}> Public Ip: 
				      					<Link  target="_blank">
				      						{VmStore.getPublicId(this.props.data.vm)}
				      					</Link>
		      					</label> : ""
			      			}
		      		</div>
		      	</label>
		      	</div>
		      </div>
		    </div>
			);
	}
}

export default SummaryInfoTab