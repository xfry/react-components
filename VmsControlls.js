import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import ApplicationStore from '../stores/ApplicationStore';
import UserStore from '../stores/UserStores';
import dispatcher from '../dispatcher/AppDispatcher';
import * as VmActions from '../actions/VmActions';
import VmStore from '../stores/VmStore';
import Utils from '../utils/AppUtils';

//backgroundColor='#00b0a8'

class AppControls extends React.Component {
  constructor(props) {
    super(props);
  }

  _onClickPlay(e) {
    console.log('current vm selected: ', VmStore.currentVm[0].id);
    dispatcher.dispatch({type: 'START_VM', id: ApplicationStore.selectedRowId})
  }

  _onClickStop(e) {
    dispatcher.dispatch({type: 'STOP_VM', id: ApplicationStore.selectedRowId})
  }

  _onClickRestart(e) {
    dispatcher.dispatch({type: 'RESTART_VM', id: ApplicationStore.selectedRowId})
  }

  _onClickDelete(e) {
    dispatcher.dispatch({type: 'DELETE_VM', id: ApplicationStore.selectedRowId})
  }

  VmControllsActions(action) {
    let auth = UserStore.userData().auth;
    let requestData = {
      'method': '',
      'path': '',
      'auth': auth,
    };

    switch(action.type) {
      case 'START_VM':
          requestData.data = {
              ids: [VmStore.currentVm[0].id]
          }
          requestData.method = "post";
          requestData.path = `applications/${Number(action.id)}/vms/start`;
          console.log("Play VM ", requestData)
          VmActions.runAction(requestData);
        break;
      case 'RESTART_VM':
          requestData.data = {
              ids: [VmStore.currentVm[0].id]
          }
          requestData.method = "post";
          requestData.path = `applications/${Number(action.id)}/vms/restart`;

          VmActions.runAction(requestData);
        break;
      case 'STOP_VM':
          requestData.data = {
              ids: [VmStore.currentVm[0].id]
          }
          requestData.method = "post";
          requestData.path = `applications/${Number(action.id)}/vms/stop`;

          VmActions.runAction(requestData);
        break;
      case 'DELETE_VM':
          Utils.showLoadingLayer();
          requestData.data = {
              ids: [VmStore.currentVm[0].id]
          }

          requestData.method = "delete";
          requestData.path = `applications/${Number(action.id)}/vms/${VmStore.currentVm[0].id}`;
          VmActions.deleteVM(requestData);
        break;
    }
  }

  componentDidMount() {
    let elements = document.querySelectorAll('.vm_btn');
    elements.forEach((item, index) => {
      item.parentElement.setAttribute('style', 'min-width: 60px; max-width: 60px; float: left; display: inline-block');
    });

    this.token = dispatcher.register(this.VmControllsActions.bind(this));
  }

  componentWillUnmount() {
    dispatcher.unregister(this.token);
  }

  componentWillMount() {
  }

  render() {
    return (
      <div>
        <RaisedButton label={ <i className="fi-play" style={{fontSize: '20px' }}></i>}
                      style={{padding: '0 1px'}}
                      containerElement={<label className="vm_btn"></label>}
                      buttonStyle={{ width: '40px', minWidth: '40px'}}
                      onTouchTap={this._onClickPlay.bind(this)}/>
        <RaisedButton label={ <i className="fi-loop" style={{fontSize: '20px' }}></i>} 
                      style={{padding: '0 1px'}}
                      containerElement={<label className="vm_btn"></label>}
                      onTouchTap={this._onClickRestart.bind(this)}/>
        <RaisedButton label={<i className="fi-stop" style={{fontSize: '20px' }}></i>}
                      style={{padding: '0 1px'}}
                      containerElement={<label className="vm_btn"></label>}
                      onTouchTap={this._onClickStop.bind(this)}/>
        <RaisedButton label={ <i className="fi-trash" style={{fontSize: '20px' }}></i>}
                      style={{padding: '0 1px'}}
                      containerElement={<label className="vm_btn"></label>}
                      onTouchTap={this._onClickDelete.bind(this)}/>
      </div>
    );
  }
}

export default AppControls;