// src/compoents/Application-list.js
import React from "react";
import moment from 'moment';
import { Link, browserHistory, hashHistory } from 'react-router';
import Divider from 'material-ui/Divider';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {List, ListItem} from 'material-ui/List';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import UserStore from "../stores/UserStores";
import * as VmActions from '../actions/VmActions';
import AppControlls from './AppControlls';
import dispatcher from '../dispatcher/AppDispatcher';
import PaginationButtons from './PaginationButtons';

import Utils from '../utils/AppUtils';

class VmsList extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      fixedHeader: true,
      fixedFooter: true,
      stripedRows: false,
      showRowHover: false,
      selectable: true,
      multiSelectable: false,
      enableSelectAll: false,
      deselectOnClickaway: true,
      showCheckboxes: true,
      height: '300px'
    };

    if(UserStore.userInfo === null) {
      UserStore.userInfo = JSON.parse(atob(localStorage.getItem("user")));
      UserStore.emit('change');
    }

    this.hideSaveLayer = this.hideSaveLayer.bind(this);
    this.showLoadingLayer = this.showLoadingLayer.bind(this);
    this.listActions     = this.listActions.bind(this);
  }

  _onRowSelection(rowNumber) {
    /*if(document.querySelector(`[data-id='${rowNumber}']`) !== null) {      
      ApplicationStore.selectedRowId = Number(document.querySelector(`[data-id='${rowNumber}']`).id);
    } else {
      ApplicationStore.selectedRowId = null;
    }*/
  }

  hideSaveLayer() {
    if(document.querySelector('.container_layer_loader')) {
      document.querySelector('.container_layer_loader').setAttribute('style', "display: none");
    }
  }

  showLoadingLayer() {
    if(document.querySelector('.container_layer_loader')) {
      document.querySelector('.container_layer_loader').setAttribute('style', "display: block");
    }
  }

  _onCellClick(rowNumber, columnNumber, e) {
    const tableRow = $("tr").find(`[data-id='${rowNumber}']`);
    const currentRow = tableRow.prevObject[rowNumber+1];
    const subpath = $(currentRow).attr('id');
    const path = 'app';

    console.log('hizo click en la celda');
  }

  componentDidMount() {
    document.getElementById("main").setAttribute("style", "background-color: white");
    document.body.setAttribute("style", "background: white");
    if(document.querySelector('.layer')) {
      document.querySelector('.layer').setAttribute('style', "display: none");
    }

    this.hideSaveLayer();
  }

  formatDate(date) {
    return moment(date).format('L');
  }

  listActions(action) {
    switch(action.type) {
      case "START_ACTION":
          this.showLoadingLayer();
          console.log('se ejecuto start actions');
          break;
      case "RECEIVE_CURRENT_APP":
          this.hideSaveLayer();
          break;
      case "PREVIOUS":
          this.setState({
            initialValue: action.data.initialValue - 10,
            nextValue: action.data.nextValue - 10
          });
          console.log('Listen previous: ', action.data);
          break;          
      case "NEXT":
          this.setState({
            initialValue: action.data.initialValue + 10,
            nextValue: action.data.nextValue + 10
          });
          console.log('Listen next: ', action.data);
          break;
    }
  }

  componentWillMount() {
    this.setState({
      initialValue: 0,
      nextValue: 10,
    });
    this.toke = dispatcher.register(this.listActions);
  }

  componentWillUnmount() {
    dispatcher.unregister(this.toke);
  }

  render() {
    return (
      <div>
        <div className="row large-12 medium-12" style={{margin: '24px 0'}}>
          <span className="medium-4 large-4 column" style={{float: 'left'}}><strong className="scenarios_number">{`${this.props.vmList.length}`}</strong> VM's </span>
          <span className="medium-8 large-8 column" style={{float: 'right', textAlign: 'right'}}>
            <PaginationButtons paginationLimit={this.props.vmList} />
          </span>
        </div>
        <MuiThemeProvider>
          <Table  onCellClick={this._onCellClick}
                  onRowSelection={this._onRowSelection.bind(this)}
                  className="table-app_list">
            <TableHeader>
              <TableRow>
                <TableHeaderColumn style={{ width: '40px'}}>
                  <i className="fi-flag" style={{fontSize: '20px'}}></i>
                </TableHeaderColumn>
                <TableHeaderColumn style={{ width: '40px'}}>
                  <i></i>
                </TableHeaderColumn>
                <TableHeaderColumn>Name</TableHeaderColumn>
                <TableHeaderColumn>Creation Date</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody>
                {
                  this.props.vmList.slice(this.state.initialValue, this.state.nextValue)
                            .map((row, index) => {
                              return (<TableRow key={index} data-id={index} id={row['id']}>
                                          <TableRowColumn style={{ width: '40px' }}>
                                            {
                                              <i  className='fi-check'
                                                  style={{fontSize: '15px', color: 'blue' }}> </i>
                                            }
                                          </TableRowColumn>
            
                                          <TableRowColumn >
                                            {`${Utils.cutString(row['name'], 25, 30)}`}
                                          </TableRowColumn>
                                          
                                          <TableRowColumn >
                                            <i className='fi-calendar' style={{ fontSize: '15px', color: '#ccc'}}></i> 
                                            <span style={{ color: '#ccc'}}> {`${this.formatDate(row['creationTime'])}`}</span>
                                          </TableRowColumn>
                                      </TableRow>)
                            })
                }
            </TableBody>
          </Table>
        </MuiThemeProvider>
      </div>
    );
  }
};

VmsList.defaultProps = {
  currentRow: null
}

export default VmsList;