import React from 'react';
import {Tabs, Tab} from 'material-ui/Tabs';
import FontIcon from 'material-ui/FontIcon';
import MapsPersonPin from 'material-ui/svg-icons/maps/person-pin';
import ApplicationStore from "../stores/ApplicationStore";
import VmStore from "../stores/VmStore";
import { Link } from 'react-router';
import dispatcher from "../dispatcher/AppDispatcher";
import moment from 'moment';
import RaisedButton from 'material-ui/RaisedButton';


import SummaryInfoTab from './SummaryInfoTab';
import GeneralInfoTab from './GeneralInfoTab';
import NetworkInfoTab from './NetworkInfoTab';
import DiskInfoTab 		from './DiskInfoTab';
import SystemInfoTab 	from './SystemInfoTab';
import ServicesInfoTab from './ServicesInfoTab';

import DropDownServices from './DropDownServices';
import FormActionsButtons from './FormActionsButton';
import VmsControlls from './VmsControlls';

function handleActive(tab) {
  alert(`A tab with this route property ${tab.props['data-route']} was activated.`);
}

const tabsTopBar = {
	textAlign: 'right',
	background: 'lavender'
}

const tabsIcon = {
	fontSize: '15px',
	padding: '0 10px',
	cursor: 'pointer'
}

class MenuSideTabs extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			summaryData: null,
			generalData: null,
			system: null,
			disk: null,
			nics: null,
			services: null,
			value: 1,
		}
		
		this.setCurrentAppState = this.setCurrentAppState.bind(this);
		this.handleChange = this.handleChange.bind(this);

		ApplicationStore.on('change', this.setCurrentAppState);
	}

	handleChange(value) {
		this.setState({
			value: value
		});
	}
	
	setCurrentAppState() {
		this.setState({
			currentApp: ApplicationStore.getCurrentApp(),
		})
	}

	componentWillMount() {
		this.token = dispatcher.register(this.getSelectedVm.bind(this));
	}
	
	componentWillUnmount() {
		ApplicationStore.removeListener('change', this.setCurrentAppState);
		dispatcher.unregister(this.token);
	}

	componentDidMount() {
		let containers = document.querySelectorAll(".tab_container");
		containers.forEach((item) => {
			item.setAttribute("style", `max-height: ${this.props.height-250}px;`);
		});
	}

	getSelectedVm(action) {
		switch(action.type) {
			case "CURRENT_VM_SELECTED":
						if(action.data) {
							this.setState({
								summaryData: this.createSummary(action.data),
								generalData: this.getGeneralData(action.data),
								system: this.generateSystem(action.data),
								disk: this.generateDisk(action.data),
								nics: this.getNetworkData(action.data),
								services: this.getServiceData(action.data),
							});
						}

						this.showTabs();
					break;
			case "GO_TO_FORM_TAB":
							this.setState({
								value: action.tab
							});
					break;
		}
	}

	showTabs() {
		document.querySelector('.elements-vms_right-menu').setAttribute('style', 'display: inline-block');
	}

	createSummary(data) {
		console.log('menu side tabs to find: ', data);
		let { name } 					= data[0];
		let { memorySize } 		= data[0];
		let { numCpus } 			= data[0];
		let { size } 					= data[0].hardDrives[0];
		let netWorkLength 		= data[0].networkConnections.length;
		let {hostnames}       = data[0];
		let {keypairName} 		= data[0];
		let networkConnections 	= data[0].networkConnections;
		let vm 							= data[0];
		
		console.log('hostnames ', hostnames);
		console.log('keypairName: ', keypairName !== undefined ? keypairName : undefined);

		const summary = {
			name: name,
			memorySize: 	memorySize,
			numCpus: 	numCpus,
			size: 	size,
			netWorkLength: 	netWorkLength,
			hostnames: hostnames ? hostnames[0]: null,
			keypairName: keypairName !== undefined ? keypairName : undefined,
			networkConnections: networkConnections,
			vm: vm
		}

		return summary;
	}

	getGeneralData(data) {
		const { name } = data[0];
		const { id } = data[0];
		const { baseVmId } = data[0];
		const { description } = data[0];
		const creationTime = moment(data[0].creationTime).format('L');
		const { hostnames } = data[0];

		const generalInfo = {
			name: name,
			description: description,
			creationTime: creationTime,
			hostnames: hostnames,
			id: id,
			baseVmId: baseVmId,
			supportsCloudInit: VmStore.currentVm[0].supportsCloudInit
		};

		console.log('cloud support: ', VmStore.currentVm[0].supportsCloudInit)

		return generalInfo;
	}

	generateSystem(data) {
		const { platform } = data[0];	
		const systemInfo = {
			platform: platform,
		};

		return systemInfo;
	}

	generateDisk(data) {
		const { hardDrives } = data[0];
		return hardDrives;
	}

	getNetworkData(data) {
		const { networkConnections } = data[0];
		return networkConnections;
	}
	
	getServiceData(data) {
		console.log("services avaliable", data);
		const { suppliedServices } = data[0];
		const { requiredServices } = data[0];

		let servicesInfo = {
			suppliedServices: suppliedServices,
			requiredServices: requiredServices
		}
		
		console.log('services from parent: ', servicesInfo);
		return servicesInfo;
	}

	_onClose(e) {
		document.querySelector('.elements-vms_right-menu').setAttribute('style', 'display: none');
		
		if(document.querySelector('.layer')) {
			document.querySelector('.layer').setAttribute('style', "display: none");
		}
	}

	_activateRequiredServices(e) {

	}

	render() {
		return (
			<div>
				<div 	className='tabs-top_bar'
							style={tabsTopBar}>
					<i 	className='fi-x' 
							style={tabsIcon} 
							onClick={this._onClose.bind(this)}></i>
				</div>
				<Tabs className="large-12 medium-12 "
							style={{color: '#333'}}
							value={this.state.value}
							onChange={this.handleChange}>
			    
			    <Tab 	label="Summary" 
			    			icon={ <span className="fi-page-multiple"></span> }
			    			data-route="/summary"
			    			value={1} 
			    			buttonStyle={{fontSize: '10px', fontWeight: '900', background: "#358ed7"}}>
			    	<div className="tab_container">
				    	{
				    		this.state.summaryData ?
				    		<SummaryInfoTab data={this.state.summaryData}/> :
				    		""
				    	}
				    	</div>
			    </Tab>

			    <Tab 	label="General"
			    			icon={ <span className="fi-widget"></span> }
			      		data-route="/general" 
			      		value={2} 
			      		buttonStyle={{fontSize: '10px', fontWeight: '900', background: "#358ed7"}}>
			      <div className="tab_container">
				      <div>
				      	<h3 style={{fontFamily: 'Open Sans, sans-serif'}} >General</h3>
				      	<hr/>
				      	<h5 style={{fontFamily: 'Open Sans, sans-serif'}}>General Properties</h5>
				      </div>
				      <div>
								{ 
									this.state.generalData ? 
									<GeneralInfoTab data={this.state.generalData}/> :
									""
								}
				      </div>
			      </div>

			    </Tab>

			    <Tab 	label="System" 
			    			icon={ <span className="fi-monitor"></span> }
			    			data-route="/system" 
			    			value={3} 
			    			buttonStyle={{fontSize: '10px', fontWeight: '900', background: "#358ed7"}}>
		    		<div className="tab_container">
		    			<div>
				      	<h3 style={{fontFamily: 'Open Sans, sans-serif'}} >System</h3>
				      	<hr/>
				      	<h5 style={{fontFamily: 'Open Sans, sans-serif'}} >System Properties</h5>
				      </div>
				    	{
				    		this.state.system ?
				    		<SystemInfoTab summaryData={this.state.summaryData} system={this.state.system}/> :
				    		""
				    	}
				    </div>
			    </Tab>
			    <Tab 	label="Disk's" 
			    			icon={ <span className="fi-database"></span> }
			    			data-route="/disk" 
			    			value={4}
			    			buttonStyle={{fontSize: '10px', fontWeight: '900', background: "#358ed7"}}>
			      <div className="tab_container">
				    	<label>
					      	<h3 style={{fontFamily: 'Open Sans, sans-serif'}}>Disk's</h3>
					      	<hr/>
					      	<h5 style={{fontFamily: 'Open Sans, sans-serif'}}>Disks added</h5>
					    </label>
		       		{
		       			this.state.disk ?
		       			<DiskInfoTab disk={this.state.disk} /> :
		       			""
		       		}
			      </div>
			    </Tab>
			    <Tab 	label="NIC's" 
			    			icon={ <span className="fi-link"></span> }
			    			value={5}
			    			buttonStyle={{fontSize: '10px', fontWeight: '900', background: "#358ed7"}}>
			      <div className="tab_container">
				      <label>
				      	<h3 style={{fontFamily: 'Open Sans, sans-serif'}}>Nic's</h3>
				      	<hr/>
				      	<h5 style={{fontFamily: 'Open Sans, sans-serif'}}>Network Information</h5>
				      </label>
			        {
			        	this.state.nics ?
			        	<NetworkInfoTab data={this.state.nics}/> :
			        	''
			        }
			      </div>
			    </Tab>

			    <Tab 	label="Services" 
			    			icon={ <span className="fi-list"></span> }
			    			data-route="/services"
			    			value={6}
			    			buttonStyle={{fontSize: '10px', fontWeight: '900', background: "#358ed7"}}>
			      
			      <div className="tab_container" >
			        <h3 style={{float: "left", fontFamily: 'Open Sans, sans-serif'}}>Services</h3>
			        <span style={{display: 'inline', float: 'right'}}>
								<DropDownServices />
			        </span>
			        
			        <hr/>
			      	{
			      		this.state.services ?
			      		<ServicesInfoTab suppliedServices={this.state.services.suppliedServices} 
			      										 requiredServices={this.state.services.requiredServices} /> :
			      		''
			      	}
			      </div>
			    </Tab>
			  </Tabs>
				
				<FormActionsButtons />
				<div className="vm_controlls">
					<VmsControlls />
				</div>
			</div>
			)
	}

};

export default MenuSideTabs;