import React from 'react';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import dispatcher from '../dispatcher/AppDispatcher';
import * as AppListActions from '../actions/AppListActions';
import ApplicationStore from '../stores/ApplicationStore';
import UserStore from '../stores/UserStores';

const styles = {
  customWidth: {
    width: 200,
  },
};

class DropDownMenuApplication extends React.Component {

  constructor(props) {
    super(props);
    this.state = {value: 1};
  }

  _handleChange(event, index, value) {
    this.setState({
      value: value
    });

    if(value === 2) {
      dispatcher.dispatch({type: '_START_APPLICATION', id: ApplicationStore.currentApp.id});
    } else if (value === 3) {
      dispatcher.dispatch({type: '_RESTART_APPLICATION', id: ApplicationStore.currentApp.id});

    } else if (value === 4) {
      dispatcher.dispatch({type: '_STOP_APPLICATION', id: ApplicationStore.currentApp.id});

    } else if (value === 5) {
      dispatcher.dispatch({type: '_DELETE_APPLICATION', id: ApplicationStore.currentApp.id})

    } else {

    }

  }

  dropDowActions(action) {
    let auth = UserStore.userData().auth;
    const requestData = {
      'method': '',
      'path': '',
      'auth': auth
    };

    switch(action.type) {
      case '_START_APPLICATION':
          requestData.method = "post";
          requestData.path = `applications/${Number(action.id)}/start`;
          
          AppListActions.runAction(requestData);
        break;
      case '_RESTART_APPLICATION':
          requestData.method = "post";
          requestData.path = `applications/${Number(action.id)}/restart`;

          AppListActions.runAction(requestData);
        break;
      case '_STOP_APPLICATION':
          requestData.method = "post";
          requestData.path = `applications/${Number(action.id)}/stop`;

          AppListActions.runAction(requestData);
        break;
      case '_DELETE_APPLICATION':
          requestData.method = "delete";
          requestData.path = `applications/${Number(action.id)}`;
          console.log('delete app');
          //AppListActions.runAction(requestData);
        break;
    }
  }

  componentWillMount() {
    this.token = dispatcher.register(this.dropDowActions.bind(this));
  }
  
  componentWillUnmount() {
    dispatcher.unregister(this.token);
  }

  render() {
    return (
      <div className="dropMenu_container">
        <DropDownMenu
          value={this.state.value}
          onChange={this._handleChange.bind(this)}
          style={styles.customWidth}
          autoWidth={false} >
            <MenuItem value={1} primaryText="Actions" />
            <MenuItem value={2} primaryText="Start Scenario" />
            <MenuItem value={3} primaryText="Restart Scenario" />
            <MenuItem value={4} primaryText="Stop Scenario" />
            <MenuItem value={5} primaryText="Delete Scenario" />
        </DropDownMenu>
      </div>
    );
  }
}

export default DropDownMenuApplication;