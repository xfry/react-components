import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import Dialog from 'material-ui/Dialog';
import * as BluePrintActions from '../actions/BluePrintActions';
import * as ApplicationActions from '../actions/ApplicationActions';
import dispatcher from '../dispatcher/AppDispatcher';
import ApplicationStore from '../stores/ApplicationStore';
import BlueprintStore from '../stores/BlueprintStore';
import VmStore from '../stores/VmStore';
import UserStore from '../stores/UserStores';
import SnapBarModal from './SnapBarModal';

class SaveBluePrintBtn extends React.Component {
	constructor(props) {
		super(props);
		this.currentApp = null;

		this.state = {
			open: false,
			disabled: false,
			name: '',
			description: '',
			openSnapBar: false,
			snapBarMessage: '',
		};

		this.pendingUpdate = false;
		this._openleModal = this._openleModal.bind(this);
		this._closeModal = this._closeModal.bind(this);
    this.createApplicationAction = this.createApplicationAction.bind(this);
    this._onTouchTap = this._onTouchTap.bind(this);
    this.getBlueprintList = this.getBlueprintList.bind(this);
    this.generateBluePrintList = this.generateBluePrintList.bind(this);
    this._handleSelectChange = this._handleSelectChange.bind(this);
    this._handleTextChange = this._handleTextChange.bind(this);
    this.createApplication = this.createApplication.bind(this);
    this.findBlueprintById = this.findBlueprintById.bind(this);
	};
	
	_handleSelectChange(e) {
		this.setState({
			baseBlueprintId: Number(e.nativeEvent.target.value)
		});

		let selectedBlueprint = BlueprintStore.findBlueprintById(Number(e.nativeEvent.target.value));
		this.setState({
			selectedBlueprint: selectedBlueprint
		});
	}

	_onTouchTap(e) {
		//Call action for save new appliacation
		this.createApplication();
	}

	_handleTextChange(e) {
		let applicationName = document.querySelector('#text-field-name').value;
		let applicationDescription = document.querySelector('#text-field-description').value;

		this.setState({
			name: applicationName,
			description: applicationDescription
		});
	}

	hideSaveLayer() {
    if(document.querySelector('.container_layer_loader')) {
      document.querySelector('.container_layer_loader').setAttribute('style', "display: none");
    }
  }

  showLoadingLayer() {
    if(document.querySelector('.container_layer_loader')) {
      document.querySelector('.container_layer_loader').setAttribute('style', "display: block");
    }
  }

 	createApplication() {
		this.showLoadingLayer();
		let auth = UserStore.userData().auth;
		let method = 'put';

    const requestData = {
      'method': 'post',
      'path': `applications`,
      'auth': auth,
      'data': {
					"name": this.state.name,
			    "description" : this.state.description,
			    //'design': {},
			    "baseBlueprintId": this.state.baseBlueprintId,
			    "costBuckets": {
			        "id": this.state.costBuckets.id
			    }
      }
    }

    if(!this.state.baseBlueprintId) {
			delete requestData.data['baseBlueprintId'];
			delete requestData.data['design'];
		}


    console.log('request object: ', requestData);
		ApplicationActions.saveApplication(requestData);
 	}
	
	createApplicationAction(action) {
		//POST https://cloud.ravellosystems.com/api/v1/applications/79627482/publishUpdates?startAllDraftVms=true
		switch(action.type) {
			case 'ERROR_GETTING_COST_BUCKET':
					this.hideSaveLayer();
					this.setState({
						openSnapBar: true,
						snapBarMessage: action.error
					});
					console.log('Error costBucket: ', action.error);
				break;
			case 'GOT_COST_BUCKET':
					this.setState({
						costBuckets: {
							id: action.data[0].id,
							creationTime: action.data[0].creationTime,
							name: action.data[0].name,
							deleted: action.data[0].deleted
						}
					});
					this.showLoadingLayer();
					this.getBlueprintList();
				break;
			case 'ERROR_SAVING_APPLICATION':
					this.hideSaveLayer();
					this.setState({
						openSnapBar: true,
						snapBarMessage: action.error
					});
					console.log('Error Saving Scenario: ', action.error);
				break;
			case 'SAVED_APPLICATION':
					this.hideSaveLayer();
					this.setState({
						openSnapBar: true,
						snapBarMessage: "Scenario Created successfully"
					});
					this.setState({open: false});//close
					ApplicationStore.addNewAppToList(action.data);
					// do some update with the applicaiton storage properties
				break;
			case 'ERROR_GETTING_BLUEPRINT':
					this.hideSaveLayer();
					this.setState({
						openSnapBar: true,
						snapBarMessage: "Error Getting Blueprint"
					});
				break;
			case 'GOT_BLUEPRINT_LIST':
					this.hideSaveLayer();
					
					this.setState({
						blueprintList: action.data,
					});
					
					ApplicationStore.blueprintList = this.state.blueprintList;

					this.generateBluePrintList(action.data);
					this.setState({open: true}); //open modal
				break;
		}
	}

	generateBluePrintList(blueprints) {
		let blueprintList = [];

		blueprints.forEach((item) => {
				blueprintList.push(<option defaultValue={item.id} key={item.id} id={item.id}>{item.name}</option>);
		});

		this.setState({
			blueprintObjects: blueprintList,
		});
	}

	findBlueprintById(id) {
		console.log('blueprint to search ', id);
		let currentBlueprint = null;
		this.state.blueprintList.forEach((item) => {
			if(item.id === id) {
				currentBlueprint = item;
			}
		});

		return currentBlueprint;
	}

	_handleBlueprintList(e) {
		console.log(e.nativeEvent.target.checked);
		if(e.nativeEvent.target.checked) {
			document.querySelector('#blueprint-list').removeAttribute('disabled');
		} else {
			document.querySelector('#blueprint-list').setAttribute('disabled', '');
		}
	}
	
	getBlueprintList() {
		this.showLoadingLayer();
		let auth = UserStore.userData().auth;
    const requestData = {
      'method': 'get',
      'path': `blueprints`,
      'auth': auth
    }

		BluePrintActions.getBlueprintList(requestData);
	}

	componentWillMount() {
		this.token1 = dispatcher.register(this.createApplicationAction.bind(this));
	}

	componentDidMount() {
	}

	componentWillUnmount() {
		dispatcher.unregister(this.token1);
	}

	_openleModal(e){
		let auth = UserStore.userData().auth;
    const requestData = {
      'method': 'get',
      'path': `costBuckets?skipDeleted=false`,
      'auth': auth,
    }

    ApplicationActions.getCostBucket(requestData);
  };

  _closeModal(e){
    this.setState({open: false});
  };

	render() {
		
		const actions = [
      <FlatButton
        label="Ok"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this._onTouchTap}
      />,

      <FlatButton
        label="Cancel"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this._closeModal}
      />
    ];

		return (
				<div className="button_container"
							style={{	float: 'right', 
												marginLeft: '10px',
												marginRight: '10px',
												textAlign: 'center' }}>
					<RaisedButton label='Create Scenario'
												backgroundColor='#84b3ff'
												style={{color: '#f7f3f3 !important'}}
												disabled={this.state.disabled}
      									onTouchTap={this._openleModal.bind(this)} />
					<Dialog
	          title="CREATE SCENARIO"
	          actions={actions}
	          modal={false}
	          open={this.state.open}
	          //onRequestClose={this._handleModal} 
	          >
	          <div>
	            <label htmlFor="">Name </label>
	            <TextField
	              id="text-field-name"
	              value={this.state.value}
	              onChange={this._handleTextChange}
	              required
	            />
	          </div>
	          <div>
	            <label htmlFor="">Description </label>
	            <TextField
	              id="text-field-description"
	              value={this.state.value}
	              onChange={this._handleTextChange}
	              required
	            />
	          </div>
	          <div>
	            <label htmlFor="">Bucket </label>
	            <TextField
	              id="text-field-bucket"
	              value={this.state.costBuckets ? this.state.costBuckets.name : ""}
	              disabled />
	          </div>
	          <div>
	          	<label htmlFor="">
	          		From blueprint: 
	          		<input 	type="text" type="checkbox"
	          						style={{margin: '0 10px'}}
	          						onChange={this._handleBlueprintList}/>
	          	</label>
	            <select name="" id="blueprint-list" disabled
	            				onChange={this._handleSelectChange}>
	            	{
	            		this.state.blueprintObjects
	            	}
	            </select>
	          </div>
	        </Dialog>
	        <SnapBarModal open={this.state.openSnapBar} message={this.state.snapBarMessage}/>
				</div>
			);
	}
}

export default SaveBluePrintBtn;