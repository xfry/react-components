import React from "react";
import ReactDOM from "react-dom";
import BreadCrumb from '../react-components/BreadCrumb';
import Canvas from '../react-components/Canvas';
import MenuSideLeft from '../react-components/MenuSideLeft';
import MenuSideRight from '../react-components/MenuSideRight';
import * as VmActions from '../actions/VmActions';
import * as AppListActions from '../actions/AppListActions';
import VmStore from '../stores/VmStore';
import DropDownMenuApplication from './DropDownMenuApplication';
import RaisedButton from 'material-ui/RaisedButton';
import UpdateAppButton from './UpdateAppButton';
import SaveBluePrintBtn from './SaveBluePrintBtn';
import CreateApplicationBtn from './CreateApplicationBtn';
import UserStore from '../stores/UserStores';
import ApplicationStore from '../stores/ApplicationStore';
import dispatcher from '../dispatcher/AppDispatcher';
import GeneralModal from './GeneralModal';

const tabsTopBar = {
	textAlign: 'right',
	background: 'lavender'
}

const tabsIcon = {
	fontSize: '15px',
	padding: '0 10px',
	cursor: 'pointer'
}

class AppCanvas extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
			vmList: [],
			totalVmList: [],
			currentAppId: this.props.params.name && Number(this.props.params.name),
			viewport: {
				width: null,
				height: null
			},
			vmErrors: null,
			hasMessages: false,
			modalMessages: []
		}
		
		this.verifyStorage  	= this.verifyStorage.bind(this);
		this.verifyCurrentApp = this.verifyCurrentApp.bind(this);
		this.setVmStore = this.setVmStore.bind(this);
		this.hideSaveLayer 	= 	this.hideSaveLayer.bind(this);
		this.showLoadingLayer 	= 	this.showLoadingLayer.bind(this);
		this.AppCanvasActions = this.AppCanvasActions.bind(this);
		this.setAppStoreProperties = this.setAppStoreProperties.bind(this);
		this.generateMessages = this.generateMessages.bind(this);
		this.setModalInformation = this.setModalInformation.bind(this);
	}

	_onClose(e) {
		document.querySelector('.menuSideLeft').setAttribute('style', 'display: none');
		document.querySelector('.elements-vms_right-menu').setAttribute('style', 'display: none');
	}

	_open() {
		document.querySelector('.menuSideLeft').setAttribute('style', 'display: block');
	}

	verifyCurrentApp() {
		if(Number(this.props.params.name) && ApplicationStore.currentApp !== null) {
			if(Number(this.props.params.name) !== ApplicationStore.currentApp.id) {
			
				let auth = UserStore.userInfo.auth;
	      const requestData = {
	        'method': 'GET',
	        'path': `applications/${this.props.params.name}`,
	        'auth': auth
	      }
				
				dispatcher.dispatch({type: 'START_ACTION'});
		    AppListActions.getAppById(requestData);
		    console.log('getting current images: ')
		    VmActions.getVmImages(requestData);
		    this.render();
		  }
		}
	}

	verifyStorage() {
		if(UserStore.userInfo === null) {
      UserStore.userInfo = JSON.parse(atob(localStorage.getItem("user")));
      ApplicationStore.currentApp = JSON.parse(atob(localStorage.getItem("currentApp")));
			
			let auth = UserStore.userInfo.auth;
      const requestData = {
        'method': 'GET',
        'path': `applications/${this.props.params.name}`,
        'auth': auth
      }

	    //call app by id action
	    dispatcher.dispatch({type: 'START_ACTION'});
	    AppListActions.getAppById(requestData);
	    VmActions.getVmImages(requestData);
	    this.render();
    }
	}

	setAppStoreProperties() {
		this.setState({
			currentAppId: Number(ApplicationStore.currentApp.id),
		});
	}

	_closeModal(modalName) {
		//ReactDOM.findDOMNode(document.getElementById(`${modalName}`)).remove();
	}

	generateMessages() {
		this.currentVms = {};
		console.log('se ejecuto el comando');

		if(VmStore.vmList === null || VmStore.vmList === undefined) {
			console.log('no tiene vms list');
			return;
		}

		VmStore.vmList.forEach((item, index) => {
			this.currentVms[`${item.id}`] = {
				name: item.name
			}
		});

		if(this.state.hasMessages === false && this.state.vmErrors === null) {
			this.setState({
				vmErrors: ApplicationStore.appMessages,
				hasMessages: true
			},

			this.setModalInformation(this.currentVms)
			);
		} else if(this.state.hasMessages) {
			this.setModalInformation(this.currentVms);
		}
	}

	setModalInformation(currentVms) {
		if(this.state.vmErrors !== null) {
				this.state.vmErrors.forEach((item, index) => {
					console.log('modal message: ', this.modalMessages);
					if(this.state.modalMessages.length > ApplicationStore.appMessages.length-1) {
						return;
					}

					this.state.modalMessages.push(
						<div key={`${item.ref+item.text}`} id={`modal${index}`}>
							<GeneralModal
														open={this.state.hasMessages} 
			          						title={`The vm named "${this.currentVms[item.ref].name}" has an error:`}
			          						description={item.text} 
			          						modalName={`modal${index}`}
			          						closeModal={this._closeModal.bind(this)} />
						</div>
					)
				});
			}
	}

	setVmStore() {
		this.setState({
			vmList: VmStore.getVmsList(),
			totalVmList: VmStore.getTotalVmList(),
			currentAppId: Number(this.props.params.name),
			canvasStore: null,
			viewport: {
				width: this.props.viewport.width,
				height: (this.props.viewport.height-4)
			}
		},
		
		this.generateMessages()
		);
	}

	hideSaveLayer() {
		if(document.querySelector('.container_layer_loader')) {
			document.querySelector('.container_layer_loader').setAttribute('style', "display: none");
		}
	}

	showLoadingLayer() {
		if(document.querySelector('.container_layer_loader')) {
			document.querySelector('.container_layer_loader').setAttribute('style', "display: block");
		}
	}
	
	AppCanvasActions(action) {
		switch(action.type) {
			case "START_ACTION":
					this.showLoadingLayer();
					break;
			case "RECEIVE_CURRENT_APP":
					this.hideSaveLayer();
					break;
		}
	}
	
	componentWillMount() {
		console.log('this state: ', this.state);

		this.verifyStorage();
		this.token = dispatcher.register(this.AppCanvasActions);
		VmStore.on('change', this.setVmStore);
		ApplicationStore.on('change', this.setAppStoreProperties);

		console.log('Count: ', VmStore.listenerCount('change'));
	}

	componentWillUnmount() {
		dispatcher.unregister(this.token);
		VmStore.removeListener('change', this.setVmStore);
		ApplicationStore.removeListener('change', this.setAppStoreProperties);
		
		this.setState({
			modalMessages: []
		});
	}

	componentDidMount() {
		this.verifyCurrentApp();
		document.querySelector(".layer").setAttribute("style","display: none");
	}

  render() {
  	if(this.state.vmErrors && this.state.hasMessages) {
  		this.generateMessages();
  	}

    return (
      <div className="canvas-app" ref="canvasApp">
      	<div className="app_submenu row large-12 medium-12">
      		<BreadCrumb className="canvas-app_breadCrumb" 
      								currentLink={this.props.params.name}
      								goback={this.props.route.path.split('/')} />

    			<DropDownMenuApplication className="canvas-app_dropMenu"/>
    			<UpdateAppButton currentAppId={this.props.params.name}/>
    			<SaveBluePrintBtn currentAppId={this.props.params.name}/>

      	</div>
      	<div className="canvas-app_container-elements" ref="containerElms">	
      		{
      			this.state.totalVmList &&
		      	<div className="elements-vms_left-menu">
		          <div 	className='tabs-top_bar'
										style={tabsTopBar}>
								<i 	className='fi-minus-circle' 
										style={tabsIcon} 
										onClick={this._onClose.bind(this)}></i>
								<i 	className='fi-eye' 
										style={tabsIcon} 
										onClick={this._open.bind(this)}></i>
							</div>
		      		<MenuSideLeft currentAppId={this.state.currentAppId} viewporOptions={this.props.viewport} totalVmList={this.state.totalVmList} />
		      	</div>
      			
      		}

      		{
      			this.state.vmErrors &&
		      	this.state.modalMessages
      		}

	      	{
		      	<Canvas canvasStore={null}
		      					width={(this.props.viewport.width)}
		      					height={(this.props.viewport.height-30)}
		      					vmList={this.state.vmList ? this.state.vmList : []}
		      					currentAppId={this.state.currentAppId && this.state.currentAppId }
		      					drawCanvas={true}/>
	      	}

	      	{
	      		this.state.viewport && 
		      	<div className="elements-vms_right-menu large-4 medium-4 column">
		      		<MenuSideRight 	height={(this.props.viewport.height-100)}/>
		      	</div>
	      	}
	     	</div>
      </div>
    );
  }
};

var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

AppCanvas.defaultProps = {
	viewport: {
		width: Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
		height: Math.max(document.documentElement.clientHeight, window.innerHeight || 0),
	}
}

AppCanvas.propTypes = {
	viewport: React.PropTypes.object
}

export default AppCanvas;