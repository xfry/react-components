import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import dispatcher from '../dispatcher/AppDispatcher';
import VmStore from '../stores/VmStore';

class AppControls extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      initialValue: 0,
      nextValue: 10,
      pagination: 10,
      paginationLimit: this.props.paginationLimit
    }
  }

  _onClickNext(e) {
    dispatcher.dispatch({type: 'NEXT', data: {
      initialValue: this.state.initialValue,
      nextValue: this.state.nextValue
    }});
  }

  _onClickPrevious(e) {
    dispatcher.dispatch({type: 'PREVIOUS', data: {
      initialValue: this.state.initialValue,
      nextValue: this.state.nextValue
    }});
  }

  paginationActions(action) {
    switch(action.type) {
      case 'NEXT':
        if(this.state.nextValue <= (this.props.paginationLimit.length-1)) {
          this.setState({
            initialValue: this.state.nextValue,
            nextValue: this.state.nextValue+this.state.pagination,
          });
        }
        break;
      case 'PREVIOUS':
        if(this.state.initialValue > 0 ) {
          this.setState({
            initialValue: this.state.initialValue - this.state.pagination,
            nextValue: this.state.nextValue - this.state.pagination,
          });
        }
        break;
    }
  }

  componentWillMount() {
    this.setState({
      initialValue: 0,
      nextValue: 10,
      paginationLimit: (this.props.paginationLimit.length-1)
    });
  }

  componentDidMount() {
    this.token = dispatcher.register(this.paginationActions.bind(this));
  }

  componentWillUnmount() {
    dispatcher.unregister(this.token); 
  }

  render() {
    return (
      <div>
        <RaisedButton label={ <i style={{fontSize: '20px' }} >
                                {`${this.state.initialValue}-${this.state.nextValue}`}
                              </i>}
                      style={{padding: '0 1px'}} />
        <RaisedButton label={ <i className="fi-previous" style={{fontSize: '20px' }}></i>} 
                      style={{padding: '0 1px'}}
                      className="previous_pagination"
                      onTouchTap={this._onClickPrevious.bind(this)}/>
        <RaisedButton label={ <i className="fi-next" style={{fontSize: '20px' }}></i>}
                      style={{padding: '0 1px'}}
                      className="next_pagination"
                      onTouchTap={this._onClickNext.bind(this)}/>
      </div>
    );
  }
}

export default AppControls;