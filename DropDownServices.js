import React from 'react';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import dispatcher from '../dispatcher/AppDispatcher';
import * as AppListActions from '../actions/AppListActions';
import ApplicationStore from '../stores/ApplicationStore';
import UserStore from '../stores/UserStores';

const styles = {
  float: 'left',
  customWidth: {
    width: 150,
  },
};

class DropDownMenuApplication extends React.Component {

  constructor(props) {
    super(props);
    this.state = {value: 1};
  }

  _handleChange(event, index, value) {
    this.setState({
      value: value
    });
    
    let supplied = document.querySelector('.create_supplied_services');
    let required = document.querySelector('.create_required_services');
    if(value === 1) {
       supplied.setAttribute('style', 'display: none');
      required.setAttribute('style', 'display: none');
    }else if(value === 2) {
      supplied.setAttribute('style', 'display: block');
      required.setAttribute('style', 'display: none');
    } else if (value === 3) {
      supplied.setAttribute('style', 'display: none');
      required.setAttribute('style', 'display: block');
    }

  }

  componentWillMount() {
  }
  
  componentWillUnmount() {
  }

  render() {
    return (
      <div className="dropMenu_container"
           style={{marginTop: '-15px', float: 'right'}}>
        <DropDownMenu
          value={this.state.value}
          onChange={this._handleChange.bind(this)}
          style={styles.customWidth}
          autoWidth={false} >
            <MenuItem value={1} primaryText="Actions" />
            <MenuItem value={2} primaryText="Add Supplied Service" />
            <MenuItem value={3} primaryText="Add Require Service" />
        </DropDownMenu>
      </div>
    );
  }
}

export default DropDownMenuApplication;