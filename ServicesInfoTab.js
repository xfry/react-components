import React from 'react';
import VmStore from '../stores/VmStore';
import dispatcher from '../dispatcher/AppDispatcher';
import IconButton from 'material-ui/IconButton';
import RaisedButton from 'material-ui/RaisedButton';
import ApplicationStore from '../stores/ApplicationStore';

let formDataSuppliedServices = [];
let formDataRequiredServices = [];

const inputStyle = {
	width: '30%',
	maxWidth: '40px',
	paddingLeft: '12px',
	paddingRight: '12px',
	float: 'left'
}

class ServicesInfoTab extends React.Component {
	constructor(props) {
		super(props);
		this.createSuppliedServices.bind(this);
		this.createRequiredServices.bind(this);
		this.servicesInfoAction = this.servicesInfoAction.bind(this);
		this._onBlurRequired = this._onBlurRequired.bind(this);
	}

	_onBlurServices(e) {
		console.log('Service supplied id: ', e.nativeEvent.target.form.id);

		formDataSuppliedServices.forEach((item) => {
			if(item.id === Number(e.nativeEvent.target.form.id) && e.nativeEvent.target.type !== 'checkbox' && e.nativeEvent.target.value !== '') {
				item[e.nativeEvent.target.getAttribute('id').split('_')[1]] = e.nativeEvent.target.value ?
																																			e.nativeEvent.target.value :
																																			VmStore.temporalVm.suppliedServices[0][e.nativeEvent.target.getAttribute('id').split('_')[1]];

			} else if(item.id === Number(e.nativeEvent.target.form.id) && e.nativeEvent.target.value !== '' && e.nativeEvent.target.tagName === 'SELECT') {
				console.log('select');
				item[e.nativeEvent.target.getAttribute('id').split('_')[1]] = e.nativeEvent.target.value ?
																																			e.nativeEvent.target.value :
																																			VmStore.temporalVm.suppliedServices[0][e.nativeEvent.target.getAttribute('id').split('_')[1]];

			} else if(item.id === Number(e.nativeEvent.target.form.id) && e.nativeEvent.target.type === 'checkbox') {
				console.log('checkbox')
				item[e.nativeEvent.target.getAttribute('id').split('_')[1]] = e.nativeEvent.target.checked;
			}

		});

		console.log('updated services supplied: ', formDataSuppliedServices);
		dispatcher.dispatch({type: 'UPDATE_SERVICES_INFO', suppliedServices: formDataSuppliedServices});
	}

	_onBlurRequired(e) {
		console.log('Service required selected: ', formDataRequiredServices);

		formDataRequiredServices.forEach((item) => {

			item.forEach((requiredService) => {
				console.log('item id: ', item.id === Number(e.nativeEvent.target.formId));
				if(requiredService.id === Number(e.nativeEvent.target.form.id) && e.nativeEvent.target.type !== 'checkbox' && e.nativeEvent.target.value !== '') {
					requiredService[e.nativeEvent.target.getAttribute('id').split('_')[1]] = e.nativeEvent.target.value ?
																																				e.nativeEvent.target.value :
																																				VmStore.temporalVm.requiredServices[0][e.nativeEvent.target.getAttribute('id').split('_')[1]];

				} else if(requiredService.id === Number(e.nativeEvent.target.form.id) && e.nativeEvent.target.value !== '' && e.nativeEvent.target.tagName === 'SELECT') {
					console.log('select');
					requiredService[e.nativeEvent.target.getAttribute('id').split('_')[1]] = e.nativeEvent.target.value ?
																																				e.nativeEvent.target.value :
																																				VmStore.temporalVm.requiredServices[0][e.nativeEvent.target.getAttribute('id').split('_')[1]];

				} else if(requiredService.id === Number(e.nativeEvent.target.form.id) && e.nativeEvent.target.type === 'checkbox') {
					console.log('checkbox')
					requiredService[e.nativeEvent.target.getAttribute('id').split('_')[1]] = e.nativeEvent.target.checked;
				}
				
			})


		});

		console.log('updated services required: ', formDataRequiredServices);
		dispatcher.dispatch({type: 'UPDATE_REQUIRED_SERVICES', requiredServices: formDataRequiredServices});
	}

	servicesInfoAction(action) {
		switch(action.type) {
			case "UPDATE_SERVICES_INFO":
					console.log('temporal vm has been updated: ', VmStore.temporalVm);
				break;
			case "UPDATE_REQUIRED_SERVICES":
					console.log('temporal vm has been updated: ', VmStore.temporalVm);
				break;
		}
	}

	createSuppliedServices(suppliedServices) {
		console.log('services supplied', suppliedServices)
		let suppliedServicesInfo = [];
		
		if(suppliedServices === undefined ) {
			return
		};
		
		/*http, https, ssh, rdp, udp, a, esp, sctp, ip*/

		suppliedServices.forEach((item, index) => {
			console.log('item information: ', item);
			suppliedServicesInfo.push(
				<div key={`${index}`}>
					<form key={`${item.id}`}
								action="" 
								id={item.id}
								name="services_form"
								style={{fontFamily: 'Open Sans, sans-serif', marginTop: '10px'}} >
						<input 	type="text" 
										id="supplied_name"
										className={item.id}
										name={ item.name }
										placeholder={ item.name }
										onBlur={this._onBlurServices.bind(this)} />
						
						<select type="text" 
										id="supplied_ip"
										className={item.ip}
										name={ item.ip }
										placeholder={ item.ip } 
										onBlur={this._onBlurServices.bind(this)} >

							<option placeholder={item.ip} defaultValue={item.ip} > {item.ip} </option>
						</select>

						<label htmlFor=""> Protocol
							<input 	type="text" 
											id="supplied_portRange"
											className={item.id}
											name={ item.name }
											placeholder={	item.protocol }
											onBlur={this._onBlurServices.bind(this)} />
						</label>

						<label htmlFor={item.name}> Port Range
							<input 	type="text"
											className={item.id}
											id="supplied_protocol"
											name={ item.name }
											placeholder={	item.portRange } 
											onBlur={this._onBlurServices.bind(this)} />
						</label>

						<label htmlFor=""> Extenral 
							<input 	type="checkbox" 
											style={{margin: '0 10px'}}
											id="supplied_external"
											onBlur={this._onBlurServices.bind(this)} />
						</label>
					</form>
				</div>
			)
		});
		
		console.log('suplied services: ', suppliedServicesInfo);
		return suppliedServicesInfo;
	}
	

	componentWillMount() {
		this.mapSuppliedServices();
		this.mapRequiredServices();
		this.token = dispatcher.register(this.servicesInfoAction);
	}

	componentWillUmount() {
		formDataSuppliedServices = [];
		formDataRequiredServices = [];
		dispatcher.unregister(this.token);
	}

 	mapSuppliedServices() {
 		if(typeof this.props.suppliedServices === 'undefined' || 
 			typeof this.props.suppliedServices === 'null') {
			return
		};
		
		let vms = ApplicationStore.currentApp.design.vms;
		vms.forEach((item) => {
			if(item.suppliedServices) {
				formDataSuppliedServices.push(item.suppliedServices.slice(0)[0]);
			}
		});

		console.log('Clon supplied services data: ', formDataSuppliedServices);
	}

	mapRequiredServices() {
		if(VmStore.temporalVm.requiredServices === undefined) {
			return
		};
		
		let vms = ApplicationStore.currentApp.design.vms;
		vms.forEach((item) => {
			console.log('clon requiredservices forEach: ', item.requiredServices);
			if(item.requiredServices) {
				formDataRequiredServices.push(item.requiredServices.slice(0));
			}
		});

		//formDataRequiredServices = this.props.requiredServices.slice(0);
		console.log('Clon required services: ', formDataRequiredServices);
	}

	createRequiredServices(requiredServices) {
		let requiredServicesInfo = [];
		
		if(requiredServices === undefined ) {
			return
		};

		requiredServices.map((item) => {
			//console.log('required services form: ', item.id);
			requiredServicesInfo.push(
				<form action="" key={`${item.id}`} id={item.id} style={{fontFamily: 'Open Sans, sans-serif', marginTop: '10px'}} >
					<label htmlFor="">
						<input 	type="text"
										id="required_name"
										placeholder={item.name}
										onBlur={this._onBlurRequired} />
						<input 	type="text"
										id="required_protocol"
										placeholder={item.protocol}
										onBlur={this._onBlurRequired} />
						<RaisedButton style={{width: '100%' }}>
							<i className="fi-trash" style={{fontSize: '1.5rem'}}></i>
						</RaisedButton>
					</label>
				</form>
			)
				
		});

		console.log('required services form: ', formDataRequiredServices);
		return requiredServicesInfo;
	}

	render() {
		return (
			<div>
				<div 	className="create_supplied_services"
							style={{display: 'none'}} >
					<form action=""
								key="form-suplied_services-1"
								style={{marginTop: '0'}}
								id={VmStore.temporalVm.id} 
								style={{fontFamily: 'Open Sans, sans-serif', marginTop: '10px'}} >

						<h5 className="tabs_subtitles" style={{fontFamily: 'Open Sans, sans-serif', marginTop: '0'}}> Create Supplied Services </h5>
						<input 	type="text" 
											id="supplied_name"
											className={VmStore.temporalVm.suppliedServices[0].id}
											name={ VmStore.temporalVm.suppliedServices[0].name }
											placeholder={ VmStore.temporalVm.suppliedServices[0].name }
											onBlur={this._onBlurServices.bind(this)} />
							
							<select 	type="text" 
											id="supplied_ip"
											className={VmStore.temporalVm.suppliedServices[0].ip}
											name={ VmStore.temporalVm.suppliedServices[0].ip }
											placeholder={ VmStore.temporalVm.suppliedServices[0].ip } 
											onBlur={this._onBlurServices.bind(this)} >

								<option placeholder={VmStore.temporalVm.suppliedServices[0].ip} defaultValue={VmStore.temporalVm.suppliedServices[0].ip} > {VmStore.temporalVm.suppliedServices[0].ip} </option>
							</select>

							<label htmlFor=""> Protocol
								<input 	type="text" 
												id="supplied_portRange"
												className={VmStore.temporalVm.suppliedServices[0].id}
												name={ VmStore.temporalVm.suppliedServices[0].name }
												placeholder={	VmStore.temporalVm.suppliedServices[0].protocol }
												onBlur={this._onBlurServices.bind(this)} />
							</label>

							<label htmlFor={VmStore.temporalVm.suppliedServices[0].name}> Port Range
								<input 	type="text"
												className={VmStore.temporalVm.suppliedServices[0].id}
												id="supplied_protocol"
												name={ VmStore.temporalVm.suppliedServices[0].name }
												placeholder={	VmStore.temporalVm.suppliedServices[0].portRange } 
												onBlur={this._onBlurServices.bind(this)} />
							</label>

							<label htmlFor=""> Extenral 
								<input 	type="checkbox" 
												style={{margin: '0 10px'}}
												id="supplied_external"
												onBlur={this._onBlurServices.bind(this)} />
							</label>
							<label>
								<RaisedButton style={{width: '100%' }} label="Save">
								</RaisedButton>
							</label>
					</form>
					<hr/>
				</div>
				
				<div 	style={{display: 'none'}}
							className="create_required_services">
					<form action=""
								key="form-suplied_services-1"
								style={{marginTop: '0'}}
								id={VmStore.temporalVm.id} >

						<h5 className="tabs_subtitles" style={{fontFamily: 'Open Sans, sans-serif', marginTop: '0'}}> Create Required Services </h5>
						<label htmlFor="">
							<input 	type="text"
											id="required_name"
											placeholder='name'
											/>
							<input 	type="text"
											id="required_protocol"
											placeholder='protocol'
											 />
							<label>
								<RaisedButton style={{width: '100%' }} label="Save">
								</RaisedButton>
							</label>
						</label>
					</form>
					<hr/>
				</div>

				<h5 className="tabs_subtitles" style={{fontFamily: 'Open Sans, sans-serif', marginTop: '0'}}> Update Supplied Services </h5>
    		{ 
    			this.props.suppliedServices ? 
    					this.createSuppliedServices(this.props.suppliedServices):
    					""
    		}
				<hr/>

	    	<h5 className="tabs_subtitles" style={{fontFamily: 'Open Sans, sans-serif', marginTop: '0'}}> Update Required Services </h5>
    		{
    			this.props.requiredServices ? 
    					this.createRequiredServices(this.props.requiredServices) :
    					""
    		}
			</div>
		);
	}
}

export default ServicesInfoTab;