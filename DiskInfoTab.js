import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import VmStore from '../stores/VmStore';
import dispatcher from '../dispatcher/AppDispatcher';

const diskInfo = [];

class DiskInfoTab extends React.Component {
	constructor(props) {
		super(props);
	}

	_onChange(e) {
		this.activateSaveLayer();
	}
	
	_onBlur(e) {
		if(e.nativeEvent.target.value !== '' && e.nativeEvent.target.value !== null && typeof e.nativeEvent.target.value !== undefined) {
			console.log('element Blur: ', e.nativeEvent.target.value);
			let el = e.nativeEvent.target;
			//validate vmId with ObjectId
			diskInfo.forEach((disk) => {
				if(Number(disk.id) === Number(el.form.id)) {
					if(el.id.split('_')[1] === "size" || el.id.split('_')[1] === "unit") {
						disk['size'][el.id.split('_')[1]] = el.value
					} else {
						disk[el.id.split('_')[1]] = el.value
					}
				}
			});
			
			console.log('object diskInfo modified: ', diskInfo);
			dispatcher.dispatch({type: 'UPDATE_DISK_INFO', diskInfo: diskInfo});
		}
	}

	_onFocus(e) {
		console.log('element Focus: ', e.nativeEvent);
		this.activateSaveLayer();
	}

	activateSaveLayer() {
		if(document.querySelector('.layer')) {
			document.querySelector('.layer').setAttribute('style', "display: block");
		}
	}

	diskActions(action) {
		switch(action.type) {
			case "UPDATE_DISK_INFO":
						VmStore.currentFormData.disksInfo = action.diskInfo;
						VmStore.temporalVm.hardDrives.forEach((item, index) => {
							if(item.id === action.diskInfo[index].id) {
								console.log('disk info: ', item);
								item['name'] 							= action.diskInfo[index].name;
								item['controller'] 				= action.diskInfo[index].controller;
								item.size.size 					= action.diskInfo[index].size.size;
								item.size.value 					= action.diskInfo[index].size.size;
								item.size.unit 						= action.diskInfo[index].size.unit;
								item['boot'] 							= action.diskInfo[index].boot;

								if(item['type'] === 'DISK') {
									delete action.diskInfo[index].baseDiskImageName;
								}

							}
						});
						console.log('VmStore Updated: ', VmStore.temporalVm.hardDrives);
					break;
		}
	}
	
	componentWillMount() {
		this.mapHardDrivesObjects();
		dispatcher.register(this.diskActions.bind(this));
	}

	mapHardDrivesObjects() {
		VmStore.temporalVm.hardDrives.forEach((item, index) => {
			diskInfo.push(item);
		});

		console.log("Disk Info: ", diskInfo);
	}

	generateChildForm(disk) {
		let diskInfo = [];
		if(this.props.disk) {
			disk.forEach((item, index) => {
					diskInfo.push(
						<div key={`${index}`}>
							<form key={`${index}`} id={`${item.id}`}>
								<div>
									<label 	htmlFor="disk_name">Name</label>
									<input 	id="disk_name" 
				      						type="text"
				      						onFocus={this._onFocus.bind(this) }
				      						onBlur={this._onBlur.bind(this) }
				      						placeholder= {item.name} />
								</div>
								{
									item.baseDiskImageName && 
									<div key={`image${index}`}>
										<label htmlFor="disk_baseDiskImageName">Image</label>
										<input 	id="disk_baseDiskImageName" 
					      							type="text"
					      							placeholder={ item.baseDiskImageName } disabled />	
									</div>
								}
								{
									item.type === 'CDROM' &&
									<div key={`cdrom${index}`}>
										<label>
											<RaisedButton className="form_btn" label="Eject" />
				            	<RaisedButton className="form_btn" label="Browse" />
										</label>
									</div>
								}
								<div key={`controller${index}`}>
									<label htmlFor="">Controller</label>
									<input 		id="disk_controller" 
				      							type="text"
				      							onFocus={ this._onFocus.bind(this) }
				      							onBlur={ this._onBlur.bind(this) }
				      							placeholder={ item.controller } />
								</div>
								{
									item.type === 'DISK' &&
									<div key={`size${index}`}>
										<label htmlFor="">Disk Size
											<input 	id="disk_size" 
					      							type="text"
					      							onFocus={this._onFocus.bind(this)}
					      							onBlur={this._onBlur.bind(this) }
					      							placeholder= {item.size.value} />
						      		<select id="disk_unit"
						      						onFocus={this._onFocus.bind(this)}
					      							onBlur={this._onBlur.bind(this) } >
						      			<option defaultValue={item.size.unit} >{item.size.unit}</option>
						      			<option value="KB">KB</option>
						      			<option value="MB">MB</option>
						      			<option value="GB">GB</option>
						      		</select>
										</label>
									</div>
								}
								{
									item.boot &&
									<div key={`boot${index}`}>
										<div>
											<label>
												<span> <strong> boot </strong> </span>
												<span> bootable </span>
											</label>
										</div>
										<div>
											<label>
												<input 	id="disk_boot" 
						      							type="checkbox"
						      							onFocus={this._onFocus.bind(this)}
					      								onBlur={this._onBlur.bind(this) } />
						      			<label>Skip CD boot</label>
											</label>
										</div>
									</div>
								}
							</form>
						</div>
					);
			});
		}

		return diskInfo;
	}

	render() {
		return (
			<div>
				{this.props.disk && this.generateChildForm(this.props.disk)}
			</div>
		);
	}

}

export default DiskInfoTab;