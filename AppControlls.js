import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import ApplicationStore from '../stores/ApplicationStore';
import UserStore from '../stores/UserStores';
import dispatcher from '../dispatcher/AppDispatcher';
import * as AppListActions from '../actions/AppListActions';
import CreateApplicationBtn from './CreateApplicationBtn';
//backgroundColor='#00b0a8'

class AppControls extends React.Component {
  constructor(props) {
    super(props);
  }

  _onClickPlay(e) {
    dispatcher.dispatch({type: 'START_APPLICATION', id: ApplicationStore.selectedRowId})
  }

  _onClickStop(e) {
    dispatcher.dispatch({type: 'STOP_APPLICATION', id: ApplicationStore.selectedRowId})
  }

  _onClickRestart(e) {
    dispatcher.dispatch({type: 'RESTART_APPLICATION', id: ApplicationStore.selectedRowId})
  }

  _onClickDelete(e) {
    dispatcher.dispatch({type: 'DELETE_APPLICATION', id: ApplicationStore.selectedRowId})
  }

  controllsActions(action) {
    let auth = UserStore.userData().auth;
    const requestData = {
      'method': '',
      'path': '',
      'auth': auth
    };

    switch(action.type) {
      case 'START_APPLICATION':
          requestData.method = "post";
          requestData.path = `applications/${Number(action.id)}/start`;
          AppListActions.runAction(requestData);
        break;
      case 'RESTART_APPLICATION':
          requestData.method = "post";
          requestData.path = `applications/${Number(action.id)}/restart`;

          AppListActions.runAction(requestData);
        break;
      case 'STOP_APPLICATION':
          requestData.method = "post";
          requestData.path = `applications/${Number(action.id)}/stop`;

          AppListActions.runAction(requestData);
        break;
      case 'DELETE_APPLICATION':
          requestData.method = "delete";
          requestData.path = `applications/${Number(action.id)}`;
          this.showLoadingLayer();
          AppListActions.deleteApp(requestData);
        break;
      case 'DELETE_DONE':
          ApplicationStore.removeAppFromStore(Number(action.id));
          this.hideSaveLayer();
        break;
    }
  }

  hideSaveLayer() {
    if(document.querySelector('.container_layer_loader')) {
      document.querySelector('.container_layer_loader').setAttribute('style', "display: none");
    }
  }

  showLoadingLayer() {
    if(document.querySelector('.container_layer_loader')) {
      document.querySelector('.container_layer_loader').setAttribute('style', "display: block");
    }
  }

  componentWillMount() {

  }

  componentDidMount() {
    this.token = dispatcher.register(this.controllsActions.bind(this));
  }

  componentWillUnmount() {
    dispatcher.unregister(this.token); 
  }

  render() {
    return (
      <div>
        <CreateApplicationBtn />

        <RaisedButton label={ <i className="fi-play" style={{fontSize: '20px' }}></i>}
                      style={{padding: '0 1px'}}
                      onTouchTap={this._onClickPlay.bind(this)}/>
        <RaisedButton label={ <i className="fi-loop" style={{fontSize: '20px' }}></i>} 
                      style={{padding: '0 1px'}}
                      onTouchTap={this._onClickRestart.bind(this)}/>
        <RaisedButton label={<i className="fi-stop" style={{fontSize: '20px' }}></i>}
                      style={{padding: '0 1px'}}
                      onTouchTap={this._onClickStop.bind(this)}/>
        <RaisedButton label={ <i className="fi-trash" style={{fontSize: '20px' }}></i>}
                      style={{padding: '0 1px'}}
                      onTouchTap={this._onClickDelete.bind(this)}/>
      </div>
    );
  }
}

export default AppControls;