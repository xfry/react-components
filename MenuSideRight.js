import React from "react";
import ReactDOM from "react-dom";
import MenuSideTabs from './MenuSideTabs';
import VmStore from "../stores/VmStore";

class MenuSideRight extends React.Component{
	constructor(props) {
		super(props);
	}

  componentDidMount() {
    this.menuSideRight = ReactDOM.findDOMNode(this.refs.menuSideRight);
  }

  render() {
    return (
      <div className="menuSideRight " ref="menuSideRight">
        <MenuSideTabs height={this.props.height}/>
      </div>
    );
  }
};

export default MenuSideRight;