// src/compoents/Application-list.js
import React from "react";
import moment from 'moment';
import { Link, browserHistory, hashHistory } from 'react-router';
import Divider from 'material-ui/Divider';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {List, ListItem} from 'material-ui/List';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import UserStore from "../stores/UserStores";
import ApplicationStore from "../stores/ApplicationStore";
import * as AppListActions from '../actions/AppListActions';
import * as VmActions from '../actions/VmActions';
import AppControlls from './AppControlls';
import dispatcher from '../dispatcher/AppDispatcher';

import Utils from '../utils/AppUtils';
import jq from "jquery";

class ApplicationList extends React.Component{
  constructor(props) {
    super(props);

    this.state = {
      fixedHeader: true,
      fixedFooter: true,
      stripedRows: false,
      showRowHover: false,
      selectable: true,
      multiSelectable: false,
      enableSelectAll: false,
      deselectOnClickaway: true,
      showCheckboxes: true,
      height: '300px',
    };

    if(UserStore.userInfo === null) {
      UserStore.userInfo = JSON.parse(atob(localStorage.getItem("user")));
      UserStore.emit('change');
    }

    this.hideSaveLayer = this.hideSaveLayer.bind(this);
    this.showLoadingLayer = this.showLoadingLayer.bind(this);
    this.listActions     = this.listActions.bind(this);
    //this.props.currentRow = null;
  }

  _onRowSelection(rowNumber) {
    if(document.querySelector(`[data-id='${rowNumber}']`) !== null) {      
      ApplicationStore.selectedRowId = Number(document.querySelector(`[data-id='${rowNumber}']`).id);
    } else {
      ApplicationStore.selectedRowId = null;
    }
  }

  hideSaveLayer() {
    if(document.querySelector('.container_layer_loader')) {
      document.querySelector('.container_layer_loader').setAttribute('style', "display: none");
    }
  }

  showLoadingLayer() {
    if(document.querySelector('.container_layer_loader')) {
      document.querySelector('.container_layer_loader').setAttribute('style', "display: block");
    }
  }

  _onCellClick(rowNumber, columnNumber, e) {
    const tableRow = $("tr").find(`[data-id='${rowNumber}']`);
    const currentRow = tableRow.prevObject[rowNumber+1];
    const subpath = $(currentRow).attr('id');
    const path = 'app';

    if(e.target.tagName === 'INPUT') {
      return;
    } else {
      //browserHistory.push(`${path}/${subpath}`);
      hashHistory.push("/"+path+"/"+subpath);
      ApplicationStore.appId = subpath;
      let auth = UserStore.userInfo.auth;
      console.log('subpath: ', subpath);
      const requestData = {
        'method': 'GET',
        'path': `applications/${subpath}`,
        'auth': auth
      }

      //call app by id action
      dispatcher.dispatch({type: 'START_ACTION'});
      AppListActions.getAppById(requestData);
      //load vms images allowed
      VmActions.getVmImages(requestData);
    }
  }

  componentDidMount() {
    document.getElementById("main").setAttribute("style", "background-color: white");
    document.body.setAttribute("style", "background: white");
    if(document.querySelector('.layer')) {
      document.querySelector('.layer').setAttribute('style', "display: none");
    }

    this.hideSaveLayer();
  }

  formatDate(date) {
    return moment(date).format('L');
  }

  listActions(action) {
    switch(action.type) {
      case "START_ACTION":
          this.showLoadingLayer();
          console.log('se ejecuto start actions');
          break;
      case "RECEIVE_CURRENT_APP":
          this.hideSaveLayer();
          break;     
    }
  }

  componentWillMount() {
    this.toke = dispatcher.register(this.listActions);
  }

  componentWillUnmount() {
    dispatcher.unregister(this.toke);
  }

  render() {
    return (
      <div>
        <div className="row large-12 medium-12" style={{margin: '24px 0'}}>
          <span className="medium-4 large-4 column" style={{float: 'left'}}><strong className="scenarios_number">{`${this.props.childRow.length}`}</strong> SCENARIOS</span>
          <span className="medium-8 large-8 column" style={{float: 'right', textAlign: 'right'}}>
            <AppControlls />
          </span>
        </div>
        <div>
        </div>
        <MuiThemeProvider>
          <Table  onCellClick={this._onCellClick}
                  onRowSelection={this._onRowSelection.bind(this)}
                  className="table-app_list">
            <TableHeader>
              <TableRow>
                <TableHeaderColumn style={{ width: '40px'}}>
                  <i className="fi-flag" style={{fontSize: '20px'}}></i>
                </TableHeaderColumn>
                <TableHeaderColumn style={{ width: '40px'}}>
                  <i></i>
                </TableHeaderColumn>
                <TableHeaderColumn>Name</TableHeaderColumn>
                <TableHeaderColumn>Creation Date</TableHeaderColumn>
                <TableHeaderColumn>Next Stop</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody>
                {
                  this.props.childRow.map((row, index) => {
                    return (<TableRow key={index} data-id={index} id={row['id']}>
                              <TableRowColumn style={{ width: '40px' }}>
                                {
                                  <i  className={`${typeof row['deployment'] === 'object' && row['deployment'].totalActiveVms ? 'fi-play' : 'fi-stop'}`}
                                      title={`${typeof row['deployment'] === 'object' && row['deployment'].totalActiveVms ? "Started" : "Stopped"}`}
                                      style={{fontSize: '15px', color: `${typeof row['deployment'] === 'object' && row['deployment'].totalActiveVms ? 'rgb(0, 176, 168)': 'orange'}`}}> </i>
                                }
                              </TableRowColumn>
          
                              <TableRowColumn style={{ width: '40px' }}>
                                {
                                  `${typeof row['deployment'] === 'object' && row['deployment'].totalActiveVms ?
                                            row['deployment'].totalActiveVms : '0' }`}
                              </TableRowColumn>
          
                              <TableRowColumn >
                                {`${Utils.cutString(row['name'], 25, 30)}`}
                              </TableRowColumn>
                              
                              <TableRowColumn >
                                <i className='fi-calendar' style={{ fontSize: '15px', color: '#ccc'}}></i> 
                                <span style={{ color: '#ccc'}}> {`${this.formatDate(row['creationTime'])}`}</span>
                              </TableRowColumn>
                              
                              {/*<TableRowColumn >
                                                              <i className='fi-torso' style={{ fontSize: '15px', color: '#ccc'}}></i>
                                                              <span style={{ color: '#ccc'}}> {`${row['owner']}`}</span>
                                                            </TableRowColumn>*/}
          
                              {/*<TableRowColumn >
                                                              {
                                                                `${typeof  row['deployment'] === 'object' ? 
                                                                          row['deployment'].cloudRegion.displayName :
                                                                          'N/A'}`
                                                              }
                                                            </TableRowColumn>*/}
          
                              <TableRowColumn >
                                {`${row['nextStopTask'] ? new Date(row['nextStopTask'].time): 'Never'}`}
                              </TableRowColumn>
                            </TableRow>)
                  })
                }
            </TableBody>
          </Table>
        </MuiThemeProvider>
      </div>
    );
  }
};

ApplicationList.defaultProps = {
  currentRow: null
}

export default ApplicationList;