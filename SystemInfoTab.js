import React from 'react';
import VmStore from '../stores/VmStore';
import dispatcher from '../dispatcher/AppDispatcher';

const formData = {

}

class SystemInfoTab extends React.Component {
	constructor(props) {
		super(props);

	}

	_onChange(e) {
		this.activateSaveLayer()
		this.setFormData(e);
	}

	_onFocus(e) {
		this.activateSaveLayer()
	}

	_onBlur(e) {
		this.activateSaveLayer()
		this.setFormData(e);
	}
	
	setFormData(e) {
		if(typeof VmStore.temporalVm[e.nativeEvent.target.getAttribute('id').split('_')[1]] === 'string' && e.nativeEvent.target.value !== '') {
			this.setPropertyValue(e);
		} else if( e.nativeEvent.target.value !== '' && e.nativeEvent.target.tagName !== 'SELECT') {
			if(e.nativeEvent.target.type === 'checkbox') {
				formData[e.nativeEvent.target.getAttribute('id').split('_')[1]] = e.nativeEvent.target.value ?
																																				e.nativeEvent.target.checked :
																																				VmStore.temporalVm[e.nativeEvent.target.getAttribute('id').split('_')[1]];
			} else {
				formData[e.nativeEvent.target.getAttribute('id').split('_')[1]] = e.nativeEvent.target.value ?
																																			Number(e.nativeEvent.target.value) :
																																			Number(VmStore.temporalVm[e.nativeEvent.target.getAttribute('id').split('_')[1]]);
			}
		} else if (e.nativeEvent.target.value !== '') {
			this.setPropertyValue(e)
		}

		console.log('form data: ', formData);

		dispatcher.dispatch({type: 'UPDATE_SYSTEM_INFO', systemInfo: formData});
	}

	setPropertyValue(e) {
		formData[e.nativeEvent.target.getAttribute('id').split('_')[1]] = e.nativeEvent.target.value ?
																																		e.nativeEvent.target.value :
																																		VmStore.temporalVm[e.nativeEvent.target.getAttribute('id').split('_')[1]];
	}
	
	activateSaveLayer() {
		if(document.querySelector('.layer')) {
			document.querySelector('.layer').setAttribute('style', "display: block");
		}
	}

	systemActions(action) {
		switch(action.type) {
			case 'UPDATE_SYSTEM_INFO':
					VmStore.temporalVm.numCpus 						= action.systemInfo['numCpus'];
					VmStore.temporalVm.memorySize.value 	= action.systemInfo['memorySize'];
					VmStore.temporalVm.memorySize.unit 		= action.systemInfo['memoryUnit'];
					VmStore.temporalVm.platform 					= action.systemInfo['platform']; 
					//VmStore.temporalVm.display 						= systemInfo['display'];
 					VmStore.temporalVm.allowNested 				= action.systemInfo['allowNested'];
					VmStore.temporalVm.powerOffOnStopTimeOut 		= action.systemInfo['powerOffOnStopTimeOut'];
					console.log('vm has changed: ', VmStore.temporalVm);
				break;
		}
	}

	mapFormDataObject() {
		formData['numCpus'] = VmStore.temporalVm.numCpus;
		formData['memorySize'] = VmStore.temporalVm.memorySize.value;
		formData['memoryUnit'] = VmStore.temporalVm.memorySize.unit;
		formData['platform'] = VmStore.temporalVm.platform;
		formData['display'] = VmStore.temporalVm.display;
		formData['allowNested'] = VmStore.temporalVm.allowNested
		formData['powerOffOnStopTimeOut'] = VmStore.temporalVm.powerOffOnStopTimeOut
	}

	componentWillMount() {
		this.token = dispatcher.register(this.systemActions.bind(this));
		this.mapFormDataObject();
	}
	
	componentWillUnmount() {
		dispatcher.unregister(this.token);
	}

	render() {
		return(
				<form className="vms-information"
							style={{fontFamily: 'Open Sans, sans-serif'}}>
      		<div>
      			<label htmlFor="system_numCpus">CPUs
	      			<input 	id="system_numCpus" 
	      							type="text"
	      							onFocus={this._onFocus.bind(this)}
	      							onBlur={this._onBlur.bind(this)}
	      							placeholder={this.props.summaryData ? `${this.props.summaryData.numCpus} CPUs` : ''} />
      			</label>
      		</div>
      		<div>
	      		<label htmlFor="system_memorySize">Mem Size
		      		<input 	id="system_memorySize" 
		      						type="text"
		      						onFocus={this._onFocus.bind(this)}
		      						onBlur={this._onBlur.bind(this)}
		      						placeholder={this.props.summaryData ? `${this.props.summaryData.memorySize.value} ${this.props.summaryData.memorySize.unit} Memory` : ''} />
		      		<select id="system_memoryUnit"
		      						onChange={this._onChange.bind(this)} >
		      			<option value="KB">KB</option>
		      			<option defaultValue="MB">MB</option>
		      			<option value="GB">GB</option>
		      		</select>
		      	</label>
      		</div>

      		<div>
	      		<label htmlFor="system_platform">Platform
		      		<select id="system_platform"
		      						onFocus={this._onFocus.bind(this)}
	      							onChange={this._onChange.bind(this)} >
		      			<option defaultValue={this.props.system && this.props.system.platform}>{this.props.system && this.props.system.platform}</option>
		      			<option value="v2-esx">V2-ESX</option>
		      		</select>
	      		</label>
      		</div>

      		<div>
	      		<label htmlFor="system_display">Display
		      		<select id="system_display"
		      						onFocus={this._onFocus.bind(this)}
	      							onChange={this._onChange.bind(this)} >
		      			<option defaultValue="Cirrus Logic">Cirrus Logic (default)</option>
		      			<option value="Standard VGA">Standard VGA</option>
		      		</select>
	      		</label>
      		</div>

      		<div>
	      		<label htmlFor="system_description">Allow Nested Virtualization
	      			<input 	id="system_allowNested"
	      						type="checkbox"
	      						onChange={this._onChange.bind(this)}/>
	      		</label>
      		</div>
      	</form>
			);
	}
}

export default SystemInfoTab