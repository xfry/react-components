import React from 'react';
import ReactDOM from 'react-dom';
import VmStore from '../stores/VmStore';
import UserStore from '../stores/UserStores';
import dispatcher from '../dispatcher/AppDispatcher';
import * as VmActions from '../actions/VmActions';

const formData = {
	vm_name: null,
	description: null,
	creationTime: null,
	hostnames: null,
	vm_id: null,
	baseVmId: null,
	supportsCloudInit: null
}

class createGeneralElement extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			keypairs: this.props.data.supportsCloudInit,
			keyPairsElements: []
		}

		console.log('data of props: ', this.props.data.supportsCloudInit);

		this.getKeyPairsData = this.getKeyPairsData.bind(this);
		this.generateKeypairElements = this.generateKeypairElements.bind(this);
		this.removeKeypairList = this.removeKeypairList.bind(this);
	}

	_onSubmit(e) {
		e.prenvetDefault();
		return false;
	}

	_onCheck(e) {
		let checked = e.nativeEvent.target.checked;
		if(!checked) {
			this.removeKeypairList();
		}

	}

	_onChange(e) {
		//console.log('ha cambiado ', e.nativeEvent.target);
		if(document.querySelector('.layer')) {
			document.querySelector('.layer').setAttribute('style', "display: block");
		}
	}

	_onFocus(e) {
		//console.log('ha cambiado ', e.nativeEvent.target);
		if(document.querySelector('.layer')) {
			document.querySelector('.layer').setAttribute('style', "display: block");
		}
	}

	_onBlur(e) {
		if(e.nativeEvent.target.value !== '') {
			formData[e.nativeEvent.target.getAttribute('id')] = e.nativeEvent.target.value
			//console.log('form data: ', formData);
			dispatcher.dispatch({type: 'UPDATED_GENERAL_INFO', generalInfo: formData});
		}
	}

	setDefaultProps() {
			formData.vm_name = this.props.data.name;
			formData.description = this.props.data.description;
			formData.creationTime = this.props.data.creationTime;
			if(this.props.data.hostnames) {
				formData.hostnames = this.props.data.hostnames[0];
			}
			formData.vm_id = this.props.data.id;
			formData.baseVmId = this.props.data.baseVmId;
			formData.supportsCloudInit = this.props.data.supportsCloudInit;
	}

	componentWillMount() {
		this.setDefaultProps();
		this.token = dispatcher.register(this.generalInfoActions.bind(this));
	}

	componentDidUpdate() {
		console.log('this state: ', this.state.keypairs);
	}

	shouldComponentUpdate() {
		if(this.props.data.supportsCloudInit) {
			this.setState({
				keypairs: this.props.data.supportsCloudInit
			},()=>{
				this.getKeyPairsData();
			});

			return true;
		}

		return false;
	}

	componentDidMount() {
		console.log('state keypair: ', this.state.keypairs);
	}

	removeKeypairList() {
		console.log('removing keypairs');
		/*this.setState({
			keypairs: false
		},() => {
				//ReacDOM.unmountComponentAtNode(this.refs.keypairSection);
			console.log('dom node: ', ReactDOM.findDOMNode(this.refs.FormVmGeneralInfo))
		});*/
	}
	
	getKeyPairsData() {
		let auth = UserStore.userData().auth;
    const requestData = {
      'method': 'get',
      'path': `keypairs/`,
      'auth': auth
    }

    VmActions.getKeyPairList(requestData);
	}

	generateKeypairElements(data) {
		data.forEach((item) => {
			this.state.keyPairsElements.push(
				<option key={`${item.id}`}
								id={`${item.id}`}
								value={`${item.name}`} > {`${item.name}`} </option>
			);
		});
	}
	
	componentWillunmount() {
		dispatcher.unregister(this.token);
	}

	generalInfoActions(action) {
		switch(action.type) {
			case 'UPDATED_GENERAL_INFO':
					VmStore.currentFormData.generalInfo = action.generalInfo;
					VmStore.temporalVm['id'] = this.props.data.id;
					VmStore.temporalVm['baseVmId'] = action.generalInfo.baseVmId;
					VmStore.temporalVm['name'] = action.generalInfo.vm_name;
					VmStore.temporalVm['description'] = action.generalInfo.description;
					VmStore.temporalVm['creationTime'] = new Date(action.generalInfo.creationTime).getTime();
					VmStore.temporalVm['hostnames'][0]  = action.generalInfo.hostnames;
					VmStore.temporalVm['supportsCloudInit']  = action.generalInfo.supportsCloudInit;
					
					console.log('temporal vm hostname: ', VmStore.temporalVm['hostnames'][0]);
					console.log('temporalVm cloud init: ', VmStore.temporalVm['supportsCloudInit']);
				break;
			case 'ERROR_GETTING_KEYPAIRS':
					console.error("Error getting keypairs: ", action.error);
				break;
			case 'RECEIVED_KEYPAIRS_DONE':
					this.generateKeypairElements(action.data);
				break;
		}
	}

	render() {
		if(this.props.data.supportsCloudInit) {

		}
		return (
			<div className="vm_general-info" ref="vmGeneralInfo" id={this.props.data.id}>
				<form className="vms-information"
							ref="FormVmGeneralInfo"
							onSubmit={this._onSubmit.bind(this)} >
		  		<div>
		  			<label htmlFor="name">Name</label>
		  			<input 	id="vm_name"
		  							type="text"
		  							placeholder={this.props.data && this.props.data.name}
		  							onFocus={this._onFocus.bind(this)}
		  							onBlur={this._onBlur.bind(this)} />
		  		</div>
		  		
		  		<div>
		    			<label htmlFor="description">description
			        	<textarea onFocus={this._onChange.bind(this)}
			        						onBlur={this._onBlur.bind(this)} 
			        						id="description" 
			        						placeholder={this.props.data && this.props.data.description}
			        						defaultValue='' />
		    			</label>
		  		</div>

		  		<div>
		    		<label htmlFor="creationTime"> Created 
		    			<span> <strong id="creationTime"> {this.props.data && this.props.data.creationTime}</strong></span>
		    		</label>
		  		</div>
		  		
		  		<div>
		    		<label 	htmlFor="creationTime">HostName</label>
		    		<input 	id="hostnames"
		    						type="text" 
		    						//placeholder={this.props.data.hostnames ? this.props.data.hostnames[0] : 'to be defined'}
		    						onFocus={this._onChange.bind(this)}
		    						onBlur={this._onBlur.bind(this)} />
		  		</div>
		  		<div ref="cloudInit">
		    		<label 	htmlFor="cloudInit">Clod Init Configuration
		    			<input 	id="cloudInit"
  						type="checkbox"
  						defaultChecked={this.state.keypairs}
  						onChange={this._onCheck.bind(this)}
  						onFocus={this._onChange.bind(this)}
  						onBlur={this._onBlur.bind(this)} />
		    		</label>

		      	{
		      		this.state.keypairs ?
			    		<div className="keypair-section" key="keypairSection" ref="keypairSection">
			    			<label htmlFor="">Key Pair</label>
			      		<select id="keypair_select"
		      							onChange={this._onChange.bind(this)}
					  						onFocus={this._onChange.bind(this)}
					  						onBlur={this._onBlur.bind(this)}>
			      			<option defaultValue={'Select an Option'}>Select an Option</option>
			      			{ /*this.state.keyPairsElements*/ }
			      		</select>
			    			<label htmlFor="" >User Data Script</label>
			    		</div> :
			    		''
		      	}
		  		</div>
		  	</form>
		  </div>
		);
	}
}
export default createGeneralElement